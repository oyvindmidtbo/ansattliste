import CoreSpotlight
import XCTest

@testable import Bekk_ansattliste

class SpotlightServiceTest: XCTestCase {
    
    private let spotlightRepositoryStub = SpotlightRepositoryStub()
    private lazy var spotlightService = SpotlightService(spotlightRepository: spotlightRepositoryStub)
    
    func test_AddEmployeesToSpotlight_calls_indexSearchableItems() async {
        await spotlightService.addEmployeesToSpotlight(getEmployees())
        
        XCTAssertTrue(spotlightRepositoryStub.hasCalledIndexSearchableItems)
    }
    
    func test_deleteEmployeesFromSpotlight_calls_deleteSearchableItems() async {
        await spotlightService.deleteEmployeesFromSpotlight(getEmployees())
        
        XCTAssertTrue(spotlightRepositoryStub.hasCalledDeleteSearchableItems)
    }
    
    func test_deleteAllEmployeesFromSpotlight_calls_deleteAllSearchableItems() async {
        await spotlightService.deleteAllEmployeesFromSpotlight()
        
        XCTAssertTrue(spotlightRepositoryStub.hasCalledDeleteAllSearchableItems)
    }
    
    func test_indexed_item_has_correct_content_type() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        
        XCTAssertEqual(spotlightRepositoryStub.searchableItems?.first?.attributeSet.contentType, UTType.contact.description)
    }
    
    func test_indexed_item_has_correct_id() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        XCTAssertEqual(spotlightRepositoryStub.searchableItems?.first?.uniqueIdentifier, "\(employee.id)")
    }
    
    func test_indexed_item_has_correct_title() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        XCTAssertEqual(spotlightRepositoryStub.searchableItems?.first?.attributeSet.title, employee.fullName)
    }
    
    func test_indexed_item_has_correct_phone_number() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        let phoneNumber = spotlightRepositoryStub.searchableItems?.first?.attributeSet.phoneNumbers
        
        XCTAssertEqual(phoneNumber!.first, employee.mobilePhone)
        XCTAssertEqual(phoneNumber!.count, 1)
    }
    
    func test_indexed_item_has_correct_email_address() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        let emailAddress = spotlightRepositoryStub.searchableItems?.first?.attributeSet.emailAddresses
        
        XCTAssertEqual(emailAddress!.first, employee.email)
        XCTAssertEqual(emailAddress!.count, 1)
    }
    
    func test_indexed_item_has_content_description_when_employee_has_title() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        
        XCTAssertEqual(spotlightRepositoryStub.searchableItems?.first?.attributeSet.contentDescription, employee.title)
    }
    
    func test_indexed_item_does_not_have_content_description_when_employee_title_is_nil() async {
        let employee = EmployeeBuilder()
            .withTitle(nil)
            .build()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        
        XCTAssertNil(spotlightRepositoryStub.searchableItems?.first?.attributeSet.contentDescription)
    }
    
    func test_indexed_item_has_image_when_employee_has_image() async {
        let employee = getEmployee()
        
        await spotlightService.addEmployeesToSpotlight([employee])
        
        XCTAssertEqual(spotlightRepositoryStub.searchableItems?.first?.attributeSet.thumbnailData, employee.image?.imageData)
    }
    
    func test_indexed_item_does_not_have_image_when_employee_image_is_nil() async {
        var employee = getEmployee()
        employee.image = nil
        
        await spotlightService.addEmployeesToSpotlight([employee])
        
        XCTAssertNil(spotlightRepositoryStub.searchableItems?.first?.attributeSet.thumbnailData)
    }
    
    func test_correct_numbers_of_items_are_indexed() async {
        let employees = getEmployees()
        
        await spotlightService.addEmployeesToSpotlight(employees)
        XCTAssertEqual(spotlightRepositoryStub.searchableItems!.count, employees.count)
    }
    
    func test_delete_one_employee_from_spotlight_has_correct_id() async {
        let employee = getEmployee()
        
        await spotlightService.deleteEmployeesFromSpotlight([employee])
        XCTAssertEqual(spotlightRepositoryStub.deletedItemsIdentifiers!.first, "\(employee.id)")
    }
    
    func test_delete_one_employee_from_spotlight_deletes_excactly_one_employee() async {
        await spotlightService.deleteEmployeesFromSpotlight([getEmployee()])
        XCTAssertEqual(spotlightRepositoryStub.deletedItemsIdentifiers!.count, 1)
    }
    
    func test_several_employees_from_spotlight_has_correct_ids() async {
        let employees = getEmployees()
        
        await spotlightService.deleteEmployeesFromSpotlight(employees)
        
        for i in 0...employees.count - 1 {
            XCTAssertEqual(spotlightRepositoryStub.deletedItemsIdentifiers![i], "\(employees[i].id)")
        }
    }
    
    func test_delete_several_employees_from_spotlight_deletes_correct_numbers() async {
        let employees = getEmployees()
        
        await spotlightService.deleteEmployeesFromSpotlight(employees)
        XCTAssertEqual(spotlightRepositoryStub.deletedItemsIdentifiers!.count, employees.count)
    }
    
    private func getEmployee() -> Employee {
        return EmployeeBuilder().build()
    }
    
    private func getEmployees() -> [Employee] {
        return PreviewData.getPreviewEmployees()
    }
    
    private final class SpotlightRepositoryStub: SpotlightRepositoryProtocol {
        
        var searchableItems: [CSSearchableItem]?
        var deletedItemsIdentifiers: [String]?
        var hasCalledIndexSearchableItems = false
        var hasCalledDeleteSearchableItems = false
        var hasCalledDeleteAllSearchableItems = false
        
        func indexSearchableItems(_ searchableItems: [CSSearchableItem]) {
            hasCalledIndexSearchableItems = true
            self.searchableItems = searchableItems
        }
        
        func deleteSearchableItems(with identifiers: [String]) {
            hasCalledDeleteSearchableItems = true
            deletedItemsIdentifiers = identifiers
        }
        
        func deleteAllSearchableItems() {
            hasCalledDeleteAllSearchableItems = true
        }
    }
}
