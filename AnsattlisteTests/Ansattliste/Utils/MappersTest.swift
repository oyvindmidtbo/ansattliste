import Contacts
import XCTest

@testable import Bekk_ansattliste

class MappersTest: XCTestCase {
    
    // MARK: mapToContact tests
    
    func test_mapToContact_creates_correct_contact_type() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.contactType, CNContactType.person)
    }
    
    func test_mapToContact_maps_given_name_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.givenName, employee.firstName)
    }
    
    func test_mapToContact_maps_family_name_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.familyName, employee.lastName)
    }
    
    func test_mapToContact_maps_email_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.emailAddresses.first!.value as String, employee.email)
        XCTAssertEqual(contact.emailAddresses.count, 1)
    }
    
    func test_mapToContact_does_not_map_email_when_it_is_empty() {
        let employee = EmployeeBuilder()
            .withEmail("")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.emailAddresses.count, 0)
    }
    
    func test_mapToContact_set_work_label_on_email_when_email_address_is_bekk_domain() {
        let employee = EmployeeBuilder()
            .withEmail("kari.nordmann@bekk.no")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.emailAddresses.first!.label, CNLabelWork)
    }
    
    func test_mapToContact_set_home_label_on_email_when_email_address_is_not_bekk_domain() {
        let employee = EmployeeBuilder()
            .withEmail("kari.nordmann@gmail.com")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.emailAddresses.first!.label, CNLabelHome)
    }
    
    func test_mapToContact_maps_organization_name_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.organizationName, employee.division)
    }
    
    func test_mapToContact_maps_phone_number_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.phoneNumbers.first!.value.stringValue, "+47\(employee.mobilePhone)")
        XCTAssertEqual(contact.phoneNumbers.first!.label, CNLabelPhoneNumberMobile)
        XCTAssertEqual(contact.phoneNumbers.count, 1)
    }
    
    func test_mapToContact_does_not_map_phone_number_when_it_is_empty() {
        let employee = EmployeeBuilder()
            .withMobilePhone("")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.phoneNumbers.count, 0)
    }
    
    func test_mapToContact_maps_birthday_correct() {
        let now = Date()
        
        let employee = EmployeeBuilder()
            .withDateOfBirth(now)
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        let actualBirthday = contact.birthday!
        let calendar = Calendar.current
        
        XCTAssertEqual(actualBirthday.year, calendar.component(.year, from: now))
        XCTAssertEqual(actualBirthday.month, calendar.component(.month, from: now))
        XCTAssertEqual(actualBirthday.day, calendar.component(.day, from: now))
        XCTAssertEqual(actualBirthday.hour, calendar.component(.hour, from: now))
        XCTAssertEqual(actualBirthday.minute, calendar.component(.minute, from: now))
        XCTAssertEqual(actualBirthday.second, calendar.component(.second, from: now))
    }
    
    func test_mapToContact_maps_postal_address_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        let actualPostalAddress = contact.postalAddresses.first! as CNLabeledValue<CNPostalAddress>
        
        XCTAssertEqual(actualPostalAddress.label, CNLabelHome)
        XCTAssertEqual(actualPostalAddress.value.street, "Veien 1")
        XCTAssertEqual(actualPostalAddress.value.postalCode, "0101")
        XCTAssertEqual(actualPostalAddress.value.city, "Oslo")
        XCTAssertEqual(contact.postalAddresses.count, 1)
    }
    
    func test_mapToContact_does_not_map_address_when_it_is_empty() {
        let employee = EmployeeBuilder()
            .withStreetAddress("")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.postalAddresses.count, 0)
    }
    
    func test_mapToContact_maps_title_and_seniority_to_job_title() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.jobTitle, "\(employee.title!) (\(employee.seniority.lowercased()))")
    }
    
    func test_mapToContact_maps_only_title_to_job_title_when_title_contains_seniority() {
        let title = "Seniorkonsulent"
        let employee = EmployeeBuilder()
            .withTitle(title)
            .withSeniority(title)
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.jobTitle, title)
    }
    
    func test_mapToContact_maps_seniority_to_job_title_when_title_is_empty() {
        let employee = EmployeeBuilder()
            .withTitle("")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.jobTitle, employee.seniority)
    }
    
    func test_mapToContact_maps_seniority_to_job_title_when_title_is_nil() {
        let employee = EmployeeBuilder()
            .withTitle(nil)
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.jobTitle, employee.seniority)
    }
    
    func test_mapToContact_maps_image_when_it_exists() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.imageData, employee.image!.imageData)
    }
    
    func test_mapToContact_does_not_map_image_when_it_does_not_exist() {
        let employee = EmployeeBuilder()
            .withImage(nil)
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertNil(contact.imageData)
    }
    
    func test_mapToContact_maps_department_name_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.departmentName, employee.department)
    }
    
    func test_mapToContact_maps_note_correct() {
        let employee = getEmployee()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.note, "Faggruppe: \(employee.practiceGroup)")
    }
    
    func test_mapToContact_does_not_map_note_when_practice_group_is_empty() {
        let employee = EmployeeBuilder()
            .withPracticeGroup("")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.note, "")
    }
    
    func test_mapToContact_does_not_map_note_when_practice_group_is_ingen() {
        let employee = EmployeeBuilder()
            .withPracticeGroup("ingen")
            .build()
        
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.note, "")
    }
    
    func test_mapToContact_maps_hire_date_correct() {
        let now = Date()
        
        let employee = EmployeeBuilder()
            .withHireDate(now)
            .build()
        let contact = Mappers.mapToContact(from: employee)
        let actualHireDate = contact.dates.first!.value
        
        let calendar = Calendar.current
        
        XCTAssertEqual(actualHireDate.year, calendar.component(.year, from: now))
        XCTAssertEqual(actualHireDate.month, calendar.component(.month, from: now))
        XCTAssertEqual(actualHireDate.day, calendar.component(.day, from: now))
        XCTAssertEqual(actualHireDate.hour, calendar.component(.hour, from: now))
        XCTAssertEqual(actualHireDate.minute, calendar.component(.minute, from: now))
        XCTAssertEqual(actualHireDate.second, calendar.component(.second, from: now))
        XCTAssertEqual(contact.dates.count, 1)
        XCTAssertEqual(contact.dates.first!.label, "ansettelsesdato")
    }
    
    func test_mapToContact_maps_url_to_ansattlisten_correct() {
        let employee = EmployeeBuilder()
            .withId(1)
            .build()
        let contact = Mappers.mapToContact(from: employee)
        
        XCTAssertEqual(contact.urlAddresses.first!.value as String, "https://ansatt.bekk.no/ansatt/1")
        XCTAssertEqual(contact.urlAddresses.first!.label, "ansattlisten")
        XCTAssertEqual(contact.urlAddresses.count, 1)
    }
    
    private func getEmployee() -> Employee {
        return EmployeeBuilder().build()
    }
}
