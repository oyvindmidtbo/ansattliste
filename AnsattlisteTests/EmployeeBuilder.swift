import Foundation

@testable import Bekk_ansattliste

class EmployeeBuilder {
    
    private var department: String
    private var seniority: String
    private var division: String
    private var practiceGroup: String
    private var hireDate: Date
    private var id: Int
    private var title: String?
    private var dateOfBirth: Date
    private var email: String
    private var mobilePhone: String
    private var firstName: String
    private var lastName: String
    private var fullName: String
    private var streetAddress: String
    private var postalNr: String
    private var postalAddress: String
    private var jobStatus: String
    private var showAddressInMapConsent: Bool
    private var image: EmployeeImage?
    
    init() {
        let employee = PreviewData.getPreviewEmployee()
        department = employee.department
        seniority = employee.seniority
        division = employee.division
        practiceGroup = employee.practiceGroup
        hireDate = employee.hireDate
        id = employee.id
        title = employee.title
        dateOfBirth = employee.dateOfBirth
        email = employee.email
        mobilePhone = employee.mobilePhone
        firstName = employee.firstName
        lastName = employee.lastName
        fullName = employee.fullName
        streetAddress = employee.streetAddress
        postalNr = employee.postalNr
        postalAddress = employee.postalAddress
        showAddressInMapConsent = employee.showAddressInMapConsent
        jobStatus = employee.jobStatus
        image = employee.image
    }
    
    func withEmail(_ email: String) -> EmployeeBuilder {
        self.email = email
        return self
    }
    
    func withMobilePhone(_ mobilePhone: String) -> EmployeeBuilder {
        self.mobilePhone = mobilePhone
        return self
    }
    
    func withTitle(_ title: String?) -> EmployeeBuilder {
        self.title = title
        return self
    }
    
    func withSeniority(_ seniority: String) -> EmployeeBuilder {
        self.seniority = seniority
        return self
    }
    
    func withStreetAddress(_ streetAddress: String) -> EmployeeBuilder {
        self.streetAddress = streetAddress
        return self
    }
    
    func withPracticeGroup(_ practiceGroup: String) -> EmployeeBuilder {
        self.practiceGroup = practiceGroup
        return self
    }
    
    func withImage(_ image: EmployeeImage?) -> EmployeeBuilder {
        self.image = image
        return self
    }
    
    func withHireDate(_ hireDate: Date) -> EmployeeBuilder {
        self.hireDate = hireDate
        return self
    }
    
    func withDateOfBirth(_ dateOfBirth: Date) -> EmployeeBuilder {
        self.dateOfBirth = dateOfBirth
        return self
    }
    
    func withId(_ id: Int) -> EmployeeBuilder {
        self.id = id
        return self
    }
    
    func build() -> Employee {
        return Employee(
            department: department,
            seniority: seniority,
            division: division,
            practiceGroup: practiceGroup,
            hireDate: hireDate,
            id: id,
            title: title,
            dateOfBirth: dateOfBirth,
            email: email,
            mobilePhone: mobilePhone,
            firstName: firstName,
            lastName: lastName,
            fullName: fullName,
            streetAddress: streetAddress,
            postalNr: postalNr,
            postalAddress: postalAddress,
            showAddressInMapConsent: showAddressInMapConsent,
            jobStatus: jobStatus,
            image: image
        )
    }
}
