import CallKit
import CoreData
import XCTest

@testable import Bekk_ansattliste

class CallDirectoryEmployeeServiceTest: XCTestCase {
    
    private let callDirectoryStorageServiceStub = CallDirectoryStorageServiceStub()
    private lazy var callDirectoryEmployeesService = CallDirectoryEmployeesService(callDirectoryStorageService: callDirectoryStorageServiceStub)
    private var inMemoryManagedObjectContext = setUpInMemoryManagedObjectContext()
    
    func test_CallDirectoryEmployee_is_created() {
        let callDirectoryEmployeeEntity = getCallDirectoryEmployeeEntity(fullName: "Ola Nordmann", id: 1, number: "99887766")
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities([callDirectoryEmployeeEntity])
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertNotNil(actualCallDirectoryEmployees)
        XCTAssertEqual(actualCallDirectoryEmployees.count, 1)
        XCTAssertEqual(actualCallDirectoryEmployees.first?.fullName, "Ola Nordmann")
    }
    
    func test_nil_numbers_are_filtered() {
        let callDirectoryEmployeeEntity = getCallDirectoryEmployeeEntity(fullName: "Ola Nordmann", id: 1, number: nil)
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities([callDirectoryEmployeeEntity])
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees, [])
    }
    
    func test_empty_numbers_are_filtered() {
        let callDirectoryEmployeeEntity = getCallDirectoryEmployeeEntity(fullName: "Ola Nordmann", id: 1, number: "")
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities([callDirectoryEmployeeEntity])
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees, [])
    }
    
    func test_invalid_numbers_are_filtered() {
        let callDirectoryEmployeeEntity1 = getCallDirectoryEmployeeEntity(fullName: "Ola Nordmann", id: 1, number: "4834232")
        let callDirectoryEmployeeEntity2 = getCallDirectoryEmployeeEntity(fullName: "Kari Nordmann", id: 2, number: "90785634a")
        let callDirectoryEmployeeEntities = [callDirectoryEmployeeEntity1, callDirectoryEmployeeEntity2]
        
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities(callDirectoryEmployeeEntities)
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees, [])
    }
    
    func test_country_code_is_added_to_number() {
        let number = "99443322"
        let callDirectoryEmployeeEntity = getCallDirectoryEmployeeEntity(fullName: "Ola Nordmann", id: 1, number: number)
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities([callDirectoryEmployeeEntity])
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees.first?.number, CXCallDirectoryPhoneNumber("47\(number)")!)
    }
    
    func test_CallDirectoryEmployees_are_sorted_by_number() {
        let callDirectoryEmployeeEntity1 = getCallDirectoryEmployeeEntity(fullName: "Nummer 3", id: 1, number: "99887766")
        let callDirectoryEmployeeEntity2 = getCallDirectoryEmployeeEntity(fullName: "Nummer 1", id: 2, number: "41424344")
        let callDirectoryEmployeeEntity3 = getCallDirectoryEmployeeEntity(fullName: "Nummer 2", id: 3, number: "90223344")
        let callDirectoryEmployeeEntities = [callDirectoryEmployeeEntity1, callDirectoryEmployeeEntity2, callDirectoryEmployeeEntity3]
        
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities(callDirectoryEmployeeEntities)
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees[0].fullName, "Nummer 1")
        XCTAssertEqual(actualCallDirectoryEmployees[1].fullName, "Nummer 2")
        XCTAssertEqual(actualCallDirectoryEmployees[2].fullName, "Nummer 3")
    }
    
    func test_nil_fullName_is_converted_to_empty_string() {
        let callDirectoryEmployeeEntity = getCallDirectoryEmployeeEntity(fullName: nil, id: 1, number: "99887766")
        
        callDirectoryStorageServiceStub.setCallDirectoryEmployeeEntities([callDirectoryEmployeeEntity])
        
        let actualCallDirectoryEmployees = callDirectoryEmployeesService.getAllCallDirectoryEmployees()
        
        XCTAssertEqual(actualCallDirectoryEmployees.first?.fullName, "")
    }
    
    private func getCallDirectoryEmployeeEntity(fullName: String?, id: Int32, number: String?) -> CallDirectoryEmployeeEntity {
        let callDirectoryEmployee = CallDirectoryEmployeeEntity(context: inMemoryManagedObjectContext)
        callDirectoryEmployee.fullName = fullName
        callDirectoryEmployee.id = id
        callDirectoryEmployee.number = number
        
        return callDirectoryEmployee
    }
    
    private final class CallDirectoryStorageServiceStub: CallDirectoryStorageServiceProtocol {
        
        private var callDirectoryEmployeeEntities: [CallDirectoryEmployeeEntity]?
        
        func getAllCallDirectoryEmployeeEntities() -> [CallDirectoryEmployeeEntity] {
            return callDirectoryEmployeeEntities!
        }
        
        func setCallDirectoryEmployeeEntities(_ callDirectoryEmployeeEntities: [CallDirectoryEmployeeEntity]) {
            self.callDirectoryEmployeeEntities = callDirectoryEmployeeEntities
        }
    }
}
