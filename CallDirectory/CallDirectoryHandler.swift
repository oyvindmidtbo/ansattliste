import CallKit

class CallDirectoryHandler: CXCallDirectoryProvider {

    override func beginRequest(with context: CXCallDirectoryExtensionContext) {
        context.delegate = self

        addAllIdentificationPhoneNumbers(to: context)

        context.completeRequest()
    }

    private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
        if context.isIncremental {
            context.removeAllIdentificationEntries()
        }
        
        for callDirectoryEmployee in CallDirectoryEmployeesService().getAllCallDirectoryEmployees() {
            context.addIdentificationEntry(withNextSequentialPhoneNumber: callDirectoryEmployee.number, label: callDirectoryEmployee.fullName)
        }
    }
}

extension CallDirectoryHandler: CXCallDirectoryExtensionContextDelegate {
    func requestFailed(for extensionContext: CXCallDirectoryExtensionContext, withError error: Error) {
        LogHelper.logError("Error during Call Directory request: \(error)")
    }
}
