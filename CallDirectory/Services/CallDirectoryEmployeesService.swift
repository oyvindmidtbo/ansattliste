import CallKit

struct CallDirectoryEmployeesService {
    
    private let callDirectoryStorageService: CallDirectoryStorageServiceProtocol
    
    init() {
        callDirectoryStorageService = CallDirectoryStorageService()
    }
    
    init(callDirectoryStorageService: CallDirectoryStorageServiceProtocol) {
        self.callDirectoryStorageService = callDirectoryStorageService
    }
    
    func getAllCallDirectoryEmployees() -> [CallDirectoryEmployee] {
        return mapToCallDirectoryEmployees(from: callDirectoryStorageService.getAllCallDirectoryEmployeeEntities())
    }
    
    private func mapToCallDirectoryEmployees(from callDirectoryEmployeeEntities: [CallDirectoryEmployeeEntity]) -> [CallDirectoryEmployee] {
        return callDirectoryEmployeeEntities
            .filter { $0.number != nil && $0.number != "" }
            .filter { isAPhoneNumber($0.number!) }
            .sorted(by: { $0.number! < $1.number! })
            .compactMap({
                if let callDirectoryNumber = CXCallDirectoryPhoneNumber("47\($0.number!)") {
                    return CallDirectoryEmployee(number: callDirectoryNumber, fullName: $0.fullName ?? "")
                } else {
                    return nil
                }
            })
    }
    
    private func isAPhoneNumber(_ number: String) -> Bool {
        return number.wholeMatch(of: /^[0-9]{8}$/) != nil
    }
}
