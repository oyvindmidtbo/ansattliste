import CoreData

protocol CallDirectoryStorageServiceProtocol {
    func getAllCallDirectoryEmployeeEntities() -> [CallDirectoryEmployeeEntity]
}

struct CallDirectoryStorageService: CallDirectoryStorageServiceProtocol {
    
    private var persistentContainer: NSPersistentContainer!
    
    init() {
        persistentContainer = NSPersistentContainer(name: "Ansattliste")
        let storeURL = URL.storeURL(for: "group.ansattliste.core.data", databaseName: "Employees")
        let storeDescription = NSPersistentStoreDescription(url: storeURL)
        persistentContainer.persistentStoreDescriptions = [storeDescription]
        
        persistentContainer.loadPersistentStores { storeDescription, error in
            if let error {
                LogHelper.logError("Could not load persistent store: \(error)")
            }
        }
    }
    
    func getAllCallDirectoryEmployeeEntities() -> [CallDirectoryEmployeeEntity] {
        return StorageService(managedObjectContext: persistentContainer.viewContext).getAllEmployeesForCallDirectory()
    }
}
