import CallKit

struct CallDirectoryEmployee: Equatable {
    let number: CXCallDirectoryPhoneNumber
    let fullName: String
}
