import UIKit

struct PreviewData {
    static func getPreviewEmployee(id: Int = 0) -> Employee {
        return Employee(
            department: "Technology",
            seniority: "Seniorkonsulent",
            division: "Bekk Consulting Oslo",
            practiceGroup: "Java",
            hireDate: ISO8601DateFormatter().date(from: "2017-08-03T00:00:00+0000") ?? Date(),
            id: id,
            title: "Fagleder Kotlin",
            dateOfBirth: ISO8601DateFormatter().date(from: "1985-08-29T00:00:00+0000") ?? Date(),
            email: "ola.nordmann@bekk.no",
            mobilePhone: "99999999",
            firstName: "Ola",
            lastName: id > 0 ? "Nordmann\(id)" : "Nordmann",
            fullName: id > 0 ? "Ola Nordmann\(id)" : "Ola Nordmann",
            streetAddress: "Veien 1",
            postalNr: "0101",
            postalAddress: "Oslo",
            showAddressInMapConsent: true,
            jobStatus: "I jobb",
            image: EmployeeImage(with: UIImage(named: "previewEmployeeImage")!)
        )
    }
    
    static func getPreviewEmployees() -> [Employee] {
        return (0...10).map { getPreviewEmployee(id: $0) }
    }
}
