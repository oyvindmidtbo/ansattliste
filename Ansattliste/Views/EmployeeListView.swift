import SwiftUI

struct EmployeeListView: View {
    
    @EnvironmentObject private var appState: AppState
    @State private var searchString = ""
    @State private var showLogOutAlert = false
    @State private var showInfoPage = false
    @State private var showFilterPage = false
    @State private var filters = Filters()
    
    private let logOutText = "Logg ut"
    
    var body: some View {
        NavigationStack {
            List(filterEmployees().sorted()) { employee in
                NavigationLink(employee.fullName, value: employee)
            }
            .navigationDestination(for: Employee.self) { employee in
                ContactViewController(employee: employee)
            }
            .navigationTitle("Ansatte")
            .listStyle(PlainListStyle())
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Menu {
                        if UIDevice.current.userInterfaceIdiom == .phone {
                            Button("Identifisere anrop") {
                                showInfoPage = true
                            }
                        }
                        Button(logOutText, role: .destructive) {
                            showLogOutAlert = true
                        }
                    } label: {
                        Image(systemName: "line.3.horizontal.circle")
                    }
                    .accessibility(label: Text("Meny"))
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        showFilterPage = true
                    } label: {
                        Image(systemName: "slider.horizontal.3")
                    }
                    .accessibility(label: Text("Filtre"))
                }
                ToolbarItem(placement: .status) {
                    StatusBarView(isLoadingData: appState.isLoadingData, isFirstUseAfterLogin: appState.isFirstUseAfterLogin, downloadedEmployeeImages: appState.downloadedEmployeeImages, numberOfEmployees: filterEmployees().count)
                }
            })
            .alert(isPresented: $showLogOutAlert) {
                Alert(title: Text(logOutText), message: Text("Vil du logge ut?"), primaryButton: .destructive(Text(logOutText)) {
                    logOut()
                }, secondaryButton: .cancel())
            }
        }
        .searchable(text: $searchString)
        .disableAutocorrection(true)
        .sheet(isPresented: $showInfoPage) {
            InfoPageView()
        }
        .sheet(isPresented: $showFilterPage) {
            FilterPageView(filters: $filters)
        }
        .sheet(isPresented: $appState.showEmployeeFromSpotlight) {
            VStack {
                RoundedRectangle(cornerRadius: 8).fill(Color.gray)
                    .frame(width: 60, height: 8)
                    .padding(.top, 8)
                ContactViewController(employee: appState.employeeFromSpotlight!)
            }
        }
    }
    
    private func filterEmployees() -> [Employee] {
        return appState.employees
            .filter { employee in
                if filters.departmentFilters.contains(where: { $0.name == employee.department && !$0.isOn }) {
                    return false
                } else if filters.seniorityFilters.contains(where: { $0.name == employee.seniority && !$0.isOn }) {
                    return false
                } else if filters.jobStatusFilters.contains(where: { $0.name == employee.jobStatus && !$0.isOn }) {
                    return false
                } else {
                    return true
                }
            }
            .filter {
                searchString.isEmpty || $0.fullName.localizedStandardContains(searchString)
            }
    }
    
    private func logOut() {
        if appState.isDemoMode {
            appState.isDemoMode = false
            appState.setEmployees([Employee]())
            UserDefaults.standard.removeObject(forKey: Constants.totalEmployeesUserDefaultsKey)
        } else {
            Task {
                await AuthenticationService(appState: appState).revokeCredentialsAndClearAppData()
            }
        }
    }
}

#if DEBUG
struct EmployeeListView_Previews: PreviewProvider {
    static var previews: some View {
        let previewUserDefaults: UserDefaults = {
            let userDefaults = UserDefaults(suiteName: "preview_user_defaults")!
            userDefaults.set(PreviewData.getPreviewEmployees().count, forKey: Constants.totalEmployeesUserDefaultsKey)
            
            return userDefaults
        }()
        
        EmployeeListView().environmentObject(
            AppState(managedObjectContext: PersistentContainer.context,
                     employees: PreviewData.getPreviewEmployees()
            )
        )
        .defaultAppStorage(previewUserDefaults)
    }
}
#endif
