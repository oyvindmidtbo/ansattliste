import CoreSpotlight
import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        if appState.isLoggedIn || appState.isDemoMode {
            TabView {
                EmployeeListView()
                    .onContinueUserActivity(CSSearchableItemActionType, perform: setEmployeeFromSpotlight)
                    .tabItem {
                        Image(systemName: "list.bullet")
                        Text("Liste")
                    }
                
                MapView(locations: appState.employeeService().getMapLocations())
                    .tabItem {
                        Image(systemName: "mappin.circle")
                        Text("Kart")
                    }
            }
        } else {
            LoginView()
        }
    }
    
    private func setEmployeeFromSpotlight(_ userActivity: NSUserActivity) {
        if let uniqueEmployeeIdentifier = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String {
            appState.employeeService().setEmployeeFromSpotlight(with: uniqueEmployeeIdentifier)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(AppState(managedObjectContext: PersistentContainer.context))
    }
}
