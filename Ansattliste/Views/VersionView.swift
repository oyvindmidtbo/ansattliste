import SwiftUI

struct VersionView: View {
    
    private let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    
    var body: some View {
        if appVersion != nil {
            Text("Versjon \(appVersion!)")
                .font(.footnote)
        }
    }
}

struct VersionView_Previews: PreviewProvider {
    static var previews: some View {
        VersionView()
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
