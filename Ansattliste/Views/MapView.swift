import MapKit
import SwiftUI

struct MapView: View {
    
    @StateObject var locationManager = LocationManager()
    
    init(locations: [Location]) {
        let annotations: [MKPointAnnotation] = locations.map { location in
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            annotation.title = location.fullName
            annotation.subtitle = location.address
            
            return annotation
        }
        
        MKMapView.appearance().addAnnotations(annotations)
    }
    
    var body: some View {
        Map(coordinateRegion: .constant(locationManager.region), interactionModes: .all, showsUserLocation: true, userTrackingMode: .none, annotationItems: [Location]()) {
            MapMarker(coordinate: $0.coordinate)
        }
        .edgesIgnoringSafeArea(.top)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(locations: [])
    }
}
