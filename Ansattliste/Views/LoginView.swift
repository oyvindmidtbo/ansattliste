import SwiftUI

struct LoginView: View {
    
    @EnvironmentObject private var appState: AppState
    
    var body: some View {
        VStack {
            Spacer()
            Text("Ansattliste for Bekk")
                .font(.title)
            Spacer()
                .frame(maxHeight: 10)
            Button(action: {
                authenticateAndDownloadEmployees()
            }) {
                Text("Logg inn")
            }
            Spacer()
                .frame(maxHeight: 10)
            Button(action: {
                startDemo()
            }) {
                Text("Demo")
            }
            Spacer()
            VersionView()
        }
    }
    
    private func authenticateAndDownloadEmployees() {
        Task.init {
            await AuthenticationService(appState: appState).authenticateAndDownloadEmployees()
        }
    }
    
    private func startDemo() {
        UserDefaults.standard.set(DemoData.getDemoEmployees().count, forKey: Constants.totalEmployeesUserDefaultsKey)
        appState.setEmployees(DemoData.getDemoEmployees())
        appState.isDemoMode = true
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView().environmentObject(AppState(managedObjectContext: PersistentContainer.context))
    }
}
