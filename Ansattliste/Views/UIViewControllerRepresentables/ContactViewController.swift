import ContactsUI
import SwiftUI

struct ContactViewController: UIViewControllerRepresentable {
    
    private let employee: Employee
    
    init(employee: Employee) {
        self.employee = employee
    }
    
    func makeUIViewController(context: Context) -> CNContactViewController {
        let viewController = CNContactViewController(forUnknownContact: Mappers.mapToContact(from: employee))
        viewController.allowsEditing = false
        
        return viewController
    }
    
    func updateUIViewController(_ uiViewController: CNContactViewController, context: UIViewControllerRepresentableContext<ContactViewController>) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }

    class Coordinator: NSObject {
        var parent: ContactViewController

        init(_ contactViewController: ContactViewController) {
            parent = contactViewController
        }
    }
}
