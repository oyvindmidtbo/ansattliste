import SwiftUI

struct FilterPageView: View {
    
    @Binding var filters: Filters
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Avdeling")) {
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.design)) {
                        Text(Filters.design)
                    }
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.managementConsulting)) {
                        Text(Filters.managementConsulting)
                    }
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.oppdrag)) {
                        Text(Filters.oppdrag)
                    }
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.salgOgAdmin)) {
                        Text(Filters.salgOgAdmin)
                    }
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.teknologi)) {
                        Text(Filters.teknologi)
                    }
                    Toggle(isOn: getIsOnForDepartment(filter: Filters.trondheim)) {
                        Text(Filters.trondheim)
                    }
                }
                Section(header: Text("Senioritet")) {
                    Toggle(isOn: getIsOnForSeniority(filter: Filters.trainee)) {
                        Text(Filters.trainee)
                    }
                    Toggle(isOn: getIsOnForSeniority(filter: Filters.konsulent)) {
                        Text(Filters.konsulent)
                    }
                    Toggle(isOn: getIsOnForSeniority(filter: Filters.senior)) {
                        Text(Filters.senior)
                    }
                    Toggle(isOn: getIsOnForSeniority(filter: Filters.manager)) {
                        Text(Filters.manager)
                    }
                }
                Section(header: Text("Status")) {
//                    Toggle(isOn: getIsOnForJobStatus(filter: Filters.ikkeStartet)) {
//                        Text(Filters.ikkeStartet)
//                    }
                    Toggle(isOn: getIsOnForJobStatus(filter: Filters.iJobb)) {
                        Text(Filters.iJobb)
                    }
                    Toggle(isOn: getIsOnForJobStatus(filter: Filters.permisjon)) {
                        Text(Filters.permisjon)
                    }
                }
                Section {
                    Button(action: {
                        filters = Filters()
                    }) {
                        Text("Nullstill")
                    }
                }
            }
            .navigationTitle("Filtre")
        }
    }
    
    private func getIsOnForDepartment(filter: String) -> Binding<Bool> {
        return $filters.departmentFilters.first(where: { $0.name.wrappedValue == filter })!.isOn
    }
    
    private func getIsOnForSeniority(filter: String) -> Binding<Bool> {
        return $filters.seniorityFilters.first(where: { $0.name.wrappedValue == filter })!.isOn
    }
    
    private func getIsOnForJobStatus(filter: String) -> Binding<Bool> {
        return $filters.jobStatusFilters.first(where: { $0.name.wrappedValue == filter })!.isOn
    }
}

struct FilterPageView_Previews: PreviewProvider {
    static var previews: some View {
        FilterPageView(filters: .constant(Filters()))
    }
}
