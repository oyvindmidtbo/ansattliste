import SwiftUI

struct InfoPageView: View {
    
    var body: some View {
        VStack {
            Text("Identifisere anrop")
                .font(.largeTitle)
                .padding(.top)
            Text("Åpne Innstillinger-appen og naviger til *Telefon* -> *Identifisere og blokkere anrop* for å tillate at appen kan identifisere ukjente anrop.")
                .padding()
            Image("info_page_screenshot")
                .resizable()
                .scaledToFit()
                .padding(.leading, 30)
                .padding(.trailing, 30)
                .shadow(radius: 5)
            Spacer()
            VersionView()
        }
    }
}

struct InfoPageView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            InfoPageView()
                .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
                .previewDisplayName("iPhone SE (2nd generation)")
            
            InfoPageView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
                .previewDisplayName("iPhone 13 Pro Max")
            
            InfoPageView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
                .previewDisplayName("iPhone 13 Pro Max")
                .preferredColorScheme(.dark)
        }
    }
}
