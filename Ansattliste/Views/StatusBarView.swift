import SwiftUI

struct StatusBarView: View {
    
    let isLoadingData: Bool
    let isFirstUseAfterLogin: Bool
    let downloadedEmployeeImages: Int
    let numberOfEmployees: Int
    @AppStorage(Constants.totalEmployeesUserDefaultsKey) private var totalEmployees: Int = 0
    
    var body: some View {
        if isLoadingData && isFirstUseAfterLogin {
            ProgressView("Laster ned ansattbilder...", value: Double(downloadedEmployeeImages), total: Double(totalEmployees))
                .font(.footnote)
        } else {
            HStack {
                if isLoadingData {
                    ProgressView()
                        .padding(.trailing, 1)
                }
                
                if numberOfEmployees >= totalEmployees {
                    Text("\(numberOfEmployees) ansatte")
                        .font(.footnote)
                } else {
                    Text("\(numberOfEmployees) av \(totalEmployees) ansatte")
                        .font(.footnote)
                }
            }
        }
    }
}

struct StatusBarView_Previews: PreviewProvider {
    static var previews: some View {
        
        let previewUserDefaults: UserDefaults = {
            let userDefaults = UserDefaults(suiteName: "preview_user_defaults")!
            userDefaults.set(500, forKey: Constants.totalEmployeesUserDefaultsKey)
            
            return userDefaults
        }()
        
        Group {
            StatusBarView(isLoadingData: false, isFirstUseAfterLogin: false, downloadedEmployeeImages: 500, numberOfEmployees: 450)
                .previewDisplayName("Antall ansatte uten lasting")
            
            StatusBarView(isLoadingData: true, isFirstUseAfterLogin: false, downloadedEmployeeImages: 500, numberOfEmployees: 500)
                .previewDisplayName("Antall ansatte med lasting")
            
            StatusBarView(isLoadingData: true, isFirstUseAfterLogin: true, downloadedEmployeeImages: 300, numberOfEmployees: 500)
                .previewDisplayName("Nedlasting av ansattbilder")
        }
        .previewLayout(.fixed(width: 300, height: 70))
        .defaultAppStorage(previewUserDefaults)
    }
}
