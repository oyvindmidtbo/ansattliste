import Auth0
import UIKit

struct EmployeeService {

    private let appState: AppState
    private let networkService: NetworkService
    private let storageService: StorageService
    private let authenticationService: AuthenticationService
    private let spotlightService: SpotlightService
    private let mapService: MapService
    
    init(appState: AppState,
         networkService: NetworkService,
         storageService: StorageService,
         authenticationService: AuthenticationService,
         spotlightService: SpotlightService,
         mapService: MapService) {
        self.appState = appState
        self.networkService = networkService
        self.storageService = storageService
        self.authenticationService = authenticationService
        self.spotlightService = spotlightService
        self.mapService = mapService
    }
    
    func getLocalEmployees() -> [Employee] {
        return storageService.getAllEmployees()
    }
    
    func getLocalEmployee(with employeeID: Int) -> Employee? {
        return storageService.getEmployee(with: employeeID)
    }
    
    func fetchAndStoreEmployees() {
        Task.init {
            await fetchAndStoreEmployeesAsync()
            await mapService.createEmployeeCoordinates()
        }
    }
    
    private func fetchAndStoreEmployeesAsync() async {
        appState.setIsLoadingData()
        
        do {
            let employeeDTOs = try await fetchEmployeeDataFromNetwork()
            await storeEmployeeData(employeeDTOs)
        } catch let error as CredentialsManagerError {
            let errorString: String?
            
            if let cause = error.cause {
                errorString = "\(cause)"
            } else {
                errorString = "\(error)"
            }
            
            LogHelper.logError("Failed to get employee data from network: \(errorString!)")
            await authenticationService.revokeCredentialsAndClearAppData()
        } catch {
            LogHelper.logError("Error while fetching employees: \(error.localizedDescription)")
            appState.setIsNotLoadingData()
        }
    }
    
    private func storeEmployeeData(_ employeeDTOs: [EmployeeDTO]) async {
        UserDefaults.standard.set(employeeDTOs.count, forKey: Constants.totalEmployeesUserDefaultsKey)
        
        let employeesFromNetwork = Mappers.mapToEmployees(from: employeeDTOs)
        let employeesOnDevice = storageService.getAllEmployees()
        
        let employeesMappedWithExistingImages = EmployeeImageService.mapEmployeesWithExistingImages(employeesWithoutImage: employeesFromNetwork, employeesOnDevice: employeesOnDevice)
        
        let employeesWithoutImage = EmployeeImageService.getEmployeesWithoutImage(from: employeesMappedWithExistingImages)
        
        appState.setEmployees(employeesMappedWithExistingImages)
        storageService.saveEmployees(employees: employeesMappedWithExistingImages)
        
        if await UIDevice.current.userInterfaceIdiom == .phone {
            storageService.saveEmployeesForCallDirectory(employees: employeesMappedWithExistingImages)
        }
        
        if employeesWithoutImage.count > 0 {
            await downloadAndStoreEmployeeImages(for: employeesWithoutImage)
        } else {
            appState.setIsNotLoadingData()
            await spotlightService.addEmployeesToSpotlight(employeesMappedWithExistingImages)
        }
        
        await deleteOldEmployees(employeesOnDevice: employeesOnDevice, employeesFromNetwork: employeesFromNetwork)
    }
    
    private func downloadAndStoreEmployeeImages(for employeesWithoutImage: [Employee]) async {
        let employeeImageService = EmployeeImageService(networkService: networkService)
        
        do {
            let employeesWithImage = try await employeeImageService.downloadEmployeeImages(for: employeesWithoutImage)
            
            if employeesWithImage.count > 0 {
                storageService.saveEmployees(employees: employeesWithImage)
            }
            let allEmployeesWithImage = storageService.getAllEmployees()
            appState.setEmployees(allEmployeesWithImage)
            appState.setIsNotLoadingData()
            await spotlightService.addEmployeesToSpotlight(allEmployeesWithImage)
        } catch {
            LogHelper.logError(error.localizedDescription)
            appState.setIsNotLoadingData()
        }
    }
    
    private func deleteOldEmployees(employeesOnDevice: [Employee], employeesFromNetwork: [Employee]) async {
        let oldEmployees = employeesOnDevice.filter({ employeeOnDevice in
            return !employeesFromNetwork.contains(where: { $0.id == employeeOnDevice.id })
        })
        
        for oldEmployee in oldEmployees {
            storageService.deleteEmployee(with: oldEmployee.id)
            
            if await UIDevice.current.userInterfaceIdiom == .phone {
                storageService.deleteCallDirectoryEmployee(with: oldEmployee.id)
            }
        }
        
        await spotlightService.deleteEmployeesFromSpotlight(oldEmployees)
    }
    
    private func fetchEmployeeDataFromNetwork() async throws -> [EmployeeDTO] {
        let bearerToken = try await authenticationService.fetchBearerToken()
        
        return try await networkService.fetchEmployees(with: bearerToken)
    }
    
    func clearEmployeeData() {
        appState.setEmployees([Employee]())
        storageService.deleteAllEmployees()
        UserDefaults.standard.set(0, forKey: Constants.totalEmployeesUserDefaultsKey)
        appState.resetNumberOfDownloadedEmployeeImages()
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            storageService.deleteAllCallDirectoryEmployees()
        }
        
        Task.init {
            await spotlightService.deleteAllEmployeesFromSpotlight()
        }
    }
    
    func setEmployeeFromSpotlight(with uniqueEmployeeIdentifier: String) {
        if let employeeId = Int(uniqueEmployeeIdentifier), let employeeFromSpotlight = getLocalEmployee(with: employeeId) {
            appState.employeeFromSpotlight = employeeFromSpotlight
            appState.showEmployeeFromSpotlight = true
        }
    }
    
    func getMapLocations() -> [Location] {
        return mapService.getLocations()
    }
}
