import UIKit

struct EmployeeImageService {
    
    private let networkService: NetworkService
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    static func mapEmployeesWithExistingImages(employeesWithoutImage: [Employee], employeesOnDevice: [Employee]) -> [Employee] {
        return employeesWithoutImage.map({ employeeWithoutImage in
            if let existingImage = getExistingEmployeeImage(for: employeeWithoutImage, from: employeesOnDevice) {
                var employeeWithImage = employeeWithoutImage
                employeeWithImage.image = existingImage
                return employeeWithImage
            } else {
                return employeeWithoutImage
            }
        })
    }
    
    static func getEmployeesWithoutImage(from employees: [Employee]) -> [Employee] {
        return employees.filter { $0.image == nil }
    }
    
    private static func getExistingEmployeeImage(for employeeFromNetwork: Employee, from employeesOnDevice: [Employee]) -> EmployeeImage? {
        return employeesOnDevice.first(where: { $0.id == employeeFromNetwork.id })?.image
    }
    
    func downloadEmployeeImages(for employeesWithoutImage: [Employee]) async throws -> [Employee] {
        let imagesData = try await networkService.downloadEmployeeImages(for: employeesWithoutImage)
        
        return mapImagesToEmployees(imagesData, employeesWithoutImage: employeesWithoutImage)
    }
    
    private func mapImagesToEmployees(_ imagesData: [Int: Data], employeesWithoutImage: [Employee]) -> [Employee] {
        return imagesData.compactMap({ imageData in
            guard !imageIsPlaceholderImage(imageData: imageData.value) else {
                return nil
            }
            
            if let employeeImage = UIImage(data: imageData.value), var employeeWithImage = employeesWithoutImage.first(where: { $0.id == imageData.key }) {
                employeeWithImage.image = EmployeeImage.init(with: employeeImage)
                return employeeWithImage
            } else {
                return nil
            }
        })
    }
    
    private func imageIsPlaceholderImage(imageData: Data) -> Bool {
        return imageData.base64EncodedString().starts(with: "iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAC+lBMVEVHcEy")
    }
}
