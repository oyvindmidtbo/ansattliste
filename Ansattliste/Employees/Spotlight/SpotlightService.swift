import CoreSpotlight

struct SpotlightService {
    
    private let spotlightRepository: SpotlightRepositoryProtocol
    
    init() {
        self.spotlightRepository = SpotlightRepository()
    }
    
    init(spotlightRepository: SpotlightRepositoryProtocol) {
        self.spotlightRepository = spotlightRepository
    }
    
    func addEmployeesToSpotlight(_ employees: [Employee]) async {
        let searchableItems = employees.map { createSearchableItem(for: $0) }
        await spotlightRepository.indexSearchableItems(searchableItems)
    }
    
    private func createSearchableItem(for employee: Employee) -> CSSearchableItem {
        let searchableAttributeSet = CSSearchableItemAttributeSet(contentType: UTType.contact)
        searchableAttributeSet.title = employee.fullName
        searchableAttributeSet.phoneNumbers = [employee.mobilePhone]
        searchableAttributeSet.emailAddresses = [employee.email]
        
        if let title = employee.title {
            searchableAttributeSet.contentDescription = title
        }
        
        if let imageData = employee.image?.imageData {
            searchableAttributeSet.thumbnailData = imageData
        }
        
        return CSSearchableItem(uniqueIdentifier: "\(employee.id)", domainIdentifier: nil, attributeSet: searchableAttributeSet)
    }
    
    func deleteEmployeesFromSpotlight(_ employees: [Employee]) async {
        await spotlightRepository.deleteSearchableItems(with: employees.map { "\($0.id)" })
    }
    
    func deleteAllEmployeesFromSpotlight() async {
        await spotlightRepository.deleteAllSearchableItems()
    }
}
