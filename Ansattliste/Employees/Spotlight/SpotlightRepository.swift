import CoreSpotlight

protocol SpotlightRepositoryProtocol {
    func indexSearchableItems(_ searchableItems: [CSSearchableItem]) async -> Void
    func deleteSearchableItems(with identifiers: [String]) async -> Void
    func deleteAllSearchableItems() async -> Void
}

struct SpotlightRepository: SpotlightRepositoryProtocol {
    func indexSearchableItems(_ searchableItems: [CSSearchableItem]) async {
        if isIndexingAvailable() {
            do {
                try await CSSearchableIndex.default().indexSearchableItems(searchableItems)
            } catch {
                LogHelper.logError("Could not index Spotlight items: \(error)")
            }
        }
    }
    
    func deleteSearchableItems(with identifiers: [String]) async {
        if isIndexingAvailable() {
            do {
                try await CSSearchableIndex.default().deleteSearchableItems(withIdentifiers: identifiers)
            } catch {
                LogHelper.logError("Could not delete Spotlight items with identifiers \(identifiers): \(error)")
            }
        }
    }
    
    func deleteAllSearchableItems() async {
        if isIndexingAvailable() {
            do {
                try await CSSearchableIndex.default().deleteAllSearchableItems()
            } catch {
                LogHelper.logError("Could not delete Spotlight items: \(error)")
            }
        }
    }
    
    private func isIndexingAvailable() -> Bool {
        if CSSearchableIndex.isIndexingAvailable() {
            return true
        } else {
            LogHelper.logInfo("Spotlight indexing is not available")
            return false
        }
    }
}
