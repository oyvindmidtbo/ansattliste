import os.log

struct LogHelper {
    static func logError(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message, file, line, logger: { logStatement in
            getLogger().error("\(logStatement)")
        })
    }
    
    static func logInfo(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message, file, line, logger: { logStatement in
            getLogger().info("\(logStatement)")
        })
    }
    
    private static func log(_ message: String, _ file: String, _ line: Int, logger: (_ logStatement: String) -> ()) {
       logger("\(message) (\(file):\(line))")
    }
    
    private static func getLogger() -> Logger {
        return Logger.init(subsystem: "ansattliste_log", category: "main")
    }
}
