import CoreLocation

struct MapService {
    
    private let appState: AppState
    
    init(appState: AppState) {
        self.appState = appState
    }
    
    typealias Coordinate = (latitude: Double, longitude: Double)
    
    func createEmployeeCoordinates() async {
#if DEBUG
        printAddressesNoLongerUsed()
#endif
        
        let coordinates = await getCoordinatesForEmployees(for: appState.employees)
        let employeesWithCoordinates = mapCoordinatesToEmployees(coordinates, employees: appState.employees)
        
        appState.setEmployees(employeesWithCoordinates)
    }
    
    private func printAddressesNoLongerUsed() {
        var coordinatesMap: [String: Coordinate] = [:]
        
        appState.employees.forEach { employee in
            let employeeAddress = "\(employee.streetAddress), \(employee.postalNr) \(employee.postalAddress)"
            
            if let existingCoordinate = Coordinates.addressMap.first(where: { $0.key == employeeAddress })?.value {
                coordinatesMap.updateValue(existingCoordinate, forKey: employeeAddress)
            }
        }
        
        print("*** Adresser ikke lenger i bruk ***")
        
        Coordinates.addressMap
            .filter { address in !coordinatesMap.keys.contains(address.key) }
            .forEach { address in print(address.key) }

        print("***")
    }
    
    private func mapCoordinatesToEmployees(_ coordinates: [Int: Coordinate], employees: [Employee]) -> [Employee] {
        return employees.map({ employee in
            if let coordinate = coordinates.first(where: { $0.key == employee.id }) {
                var newEmployee = employee
                newEmployee.latitude = coordinate.value.latitude
                newEmployee.longitude = coordinate.value.longitude
                return newEmployee
            } else {
                return employee
            }
        })
    }
    
    private func getCoordinatesForEmployees(for employees: [Employee]) async -> [Int: Coordinate] {
        await withTaskGroup(of: (Int, Coordinate?).self) { group in
            for employee in employees {
                group.addTask {
                    return (employee.id, await self.createEmployeeCoordinate(for: employee))
                }
            }
            
            var coordinates = [Int: Coordinate]()
            
            for await (employeeId, coordinate) in group {
                coordinates[employeeId] = coordinate
            }
            
            return coordinates
        }
    }
    
    private func createEmployeeCoordinate(for employee: Employee) async -> Coordinate? {
        if employee.latitude != nil && employee.longitude != nil {
            return Coordinate(employee.latitude!, employee.longitude!)
        }
        
        guard employee.streetAddress != "" else {
            return nil
        }
        
        let employeeAddress = "\(employee.streetAddress), \(employee.postalNr) \(employee.postalAddress)"
        
        if let existingCoordinate = Coordinates.addressMap.first(where: { $0.key == employeeAddress })?.value {
            return existingCoordinate
        }
        
        let geocoder = CLGeocoder()
        let placemark: CLPlacemark?
        
        do {
            placemark = try await geocoder.geocodeAddressString(employeeAddress).first
        } catch {
            geocoder.cancelGeocode()
            LogHelper.logInfo("Could not generate lat and long for employee: \(employee.fullName) (\(employee.streetAddress), \(employee.postalNr) \(employee.postalAddress)). Error: \(error.localizedDescription)")
            return nil
        }
        
        let latitude = placemark?.location?.coordinate.latitude
        let longitude = placemark?.location?.coordinate.longitude
        
        guard latitude != nil, longitude != nil, latitude != 0, longitude != 0 else {
            LogHelper.logInfo("Could not generate lat and long for employee: \(employee.fullName)")
            return nil
        }
        
#if DEBUG
        print("\"\(employee.streetAddress), \(employee.postalNr) \(employee.postalAddress)\": Coordinate(\(latitude!), \(longitude!)), // \(employee.fullName)")
#endif
        return Coordinate(latitude!, longitude!)
    }
    
    func getLocations() -> [Location] {
        return appState.employees.compactMap({ employee in
            guard employee.showAddressInMapConsent == true else {
                return nil
            }
            
            if let latitude = employee.latitude, let longitude = employee.longitude {
                let address = "\(employee.streetAddress), \(employee.postalNr) \(employee.postalAddress)"
                return Location(fullName: employee.fullName, address: address, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
            } else {
                return nil
            }
        })
    }
}
