import Foundation

enum NetworkError: Error {
    case invalidServerResponse
}
