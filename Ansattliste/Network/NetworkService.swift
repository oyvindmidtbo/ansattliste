import Foundation

struct NetworkService {
    
    private let appState: AppState
    
    init(appState: AppState) {
        self.appState = appState
    }
    
    func fetchEmployees(with bearerToken: String) async throws -> [EmployeeDTO] {
        let url = URL(string: "https://api.bekk.no/employee-svc/v3/employees/with-current-history")!
        
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("Bearer \(bearerToken)", forHTTPHeaderField: "Authorization")
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else {
            throw NetworkError.invalidServerResponse
        }
        
        return try JSONDecoder().decode([EmployeeDTO].self, from: Data(data))
    }
    
    func downloadEmployeeImages(for employees: [Employee]) async throws -> [Int: Data] {
        try await withThrowingTaskGroup(of: (Int, Data).self) { group in
            for employee in employees {
                group.addTask {
                    return (employee.id, try await self.downloadEmployeeImage(for: employee.id))
                }
            }
            
            var imagesData = [Int: Data]()
                            
            for try await (employeeId, imageData) in group {
                imagesData[employeeId] = imageData
            }
            
            return imagesData
        }
    }
    
    private func downloadEmployeeImage(for employeeId: Int) async throws -> Data {
        let url = URL(string: "https://res.cloudinary.com/bekkimg/w_300/w_200,h_200,c_thumb,g_face/d_default_image.png/\(employeeId)")!
        
        let (data, response) = try await URLSession.shared.data(for: URLRequest(url: url))
        guard (response as? HTTPURLResponse)?.statusCode == 200 else {
            throw createError("Could not get image from URL: \(url.absoluteString)")
        }
        
        appState.incrementNumberOfDownloadedEmployeeImages()
        
        return data
    }
    
    private func createError(_ description: String) -> NSError {
        return NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: description])
    }
}
