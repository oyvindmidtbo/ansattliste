import Contacts

struct Mappers {
    
    static func mapToEmployees(from employeeEntities: [EmployeeEntity]) -> [Employee] {
        return employeeEntities.map { mapToEmployee(from: $0) }
    }
    
    static func mapToEmployees(from employeeDTOs: [EmployeeDTO]) -> [Employee] {
        return employeeDTOs.map { mapToEmployee(from: $0) }
    }
    
    static func mapToEmployee(from employeeEntity: EmployeeEntity) -> Employee {
        return Employee(
            department: employeeEntity.department ?? "",
            seniority: employeeEntity.seniority ?? "",
            division: employeeEntity.division ?? "",
            practiceGroup: employeeEntity.practiceGroup ?? "",
            hireDate: employeeEntity.hireDate ?? Date(),
            id: Int(employeeEntity.id),
            title: employeeEntity.title ?? "",
            dateOfBirth: employeeEntity.dateOfBirth ?? Date(),
            email: employeeEntity.email ?? "",
            mobilePhone: employeeEntity.mobilePhone ?? "",
            firstName: employeeEntity.firstName ?? "",
            lastName: employeeEntity.lastName ?? "",
            fullName: employeeEntity.fullName ?? "",
            streetAddress: employeeEntity.streetAddress ?? "",
            postalNr: employeeEntity.postalNr ?? "",
            postalAddress: employeeEntity.postalAddress ?? "",
            showAddressInMapConsent: employeeEntity.showAddressInMapConsent,
            jobStatus: employeeEntity.jobStatus ?? "",
            image: employeeEntity.image != nil ? EmployeeImage(with: employeeEntity.image!) : nil
        )
    }
    
    static func mapToContact(from employee: Employee) -> CNContact {
        let contact = CNMutableContact()
        
        contact.contactType = .person
        contact.givenName = employee.firstName
        contact.familyName = employee.lastName
        
        if employee.email != "" {
            contact.emailAddresses = getEmailAddress(for: employee)
        }
        
        contact.organizationName = employee.division
        
        if employee.mobilePhone != "" {
            contact.phoneNumbers = [CNLabeledValue<CNPhoneNumber>(label: CNLabelPhoneNumberMobile, value: CNPhoneNumber(stringValue: "+47\(employee.mobilePhone)"))]
        }
        
        contact.birthday = getDateComponents(from: employee.dateOfBirth)
        
        if employee.streetAddress != "" {
            contact.postalAddresses = [CNLabeledValue<CNPostalAddress>(label: CNLabelHome, value: getPostalAddress(for: employee))]
        }
        
        if let title = employee.title, title != "" {
            if title.lowercased().contains(employee.seniority.lowercased()) {
                contact.jobTitle = title
            } else {
                contact.jobTitle = "\(title) (\(employee.seniority.lowercased()))"
            }
        } else {
            contact.jobTitle = employee.seniority
        }
        
        if let image = employee.image {
            contact.imageData = image.imageData!
        }
        
        contact.departmentName = employee.department
        
        if employee.practiceGroup != "" && employee.practiceGroup.lowercased() != "ingen" {
            contact.note = "Faggruppe: \(employee.practiceGroup)"
        }
        
        contact.dates = getHireDate(for: employee)
        
        contact.urlAddresses = [CNLabeledValue<NSString>(label: "ansattlisten", value: NSString(string: "https://ansatt.bekk.no/ansatt/\(employee.id)"))]
        
        return contact
    }
    
    private static func getEmailAddress(for employee: Employee) -> [CNLabeledValue<NSString>] {
        var label: String
        
        if employee.email.hasSuffix("@bekk.no") {
            label = CNLabelWork
        } else {
            label = CNLabelHome
        }
        
        return [CNLabeledValue<NSString>(label: label, value: NSString(string: employee.email))]
    }
    
    private static func getPostalAddress(for employee: Employee) -> CNPostalAddress {
        let postalAddress = CNMutablePostalAddress()
        postalAddress.street = employee.streetAddress
        postalAddress.postalCode = employee.postalNr
        postalAddress.city = employee.postalAddress.capitalized
        
        return postalAddress
    }
    
    private static func getHireDate(for employee: Employee) -> [CNLabeledValue<NSDateComponents>] {
        let hireDateComponents = getDateComponents(from: employee.hireDate)
        let labeledValue = CNLabeledValue<NSDateComponents>.init(label: "ansettelsesdato", value: hireDateComponents as NSDateComponents)
        
        return [labeledValue]
    }
    
    private static func getDateComponents(from date: Date) -> DateComponents {
        var date = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute, .second], from: date)
        date.calendar = Calendar.current
        return date
    }
    
    private static func mapToEmployee(from employeeDTO: EmployeeDTO) -> Employee {
        return Employee(
            department: employeeDTO.department ?? "",
            seniority: employeeDTO.seniority ?? "",
            division: employeeDTO.division ?? "",
            practiceGroup: employeeDTO.practiceGroup ?? "",
            hireDate: mapToDate(from: employeeDTO.hireDate),
            id: employeeDTO.id,
            title: employeeDTO.title == "" ? nil : employeeDTO.title,
            dateOfBirth: mapToDate(from: employeeDTO.dateOfBirth),
            email: employeeDTO.email ?? "",
            mobilePhone: removeWhitespace(from: employeeDTO.mobilePhone),
            firstName: employeeDTO.firstName ?? "",
            lastName: employeeDTO.lastName ?? "",
            fullName: employeeDTO.name ?? "",
            streetAddress: employeeDTO.streetAddress ?? "",
            postalNr: employeeDTO.postalNr ?? "",
            postalAddress: employeeDTO.postalAddress ?? "",
            showAddressInMapConsent: employeeDTO.showAddressInMapConsent ?? false,
            jobStatus: employeeDTO.jobStatus ?? ""
        )
    }
    
    private static func removeWhitespace(from phoneNumber: String?) -> String {
        return phoneNumber?.replacingOccurrences(of: " ", with: "") ?? ""
    }
    
    private static func mapToDate(from date: String?) -> Date {
        return ISO8601DateFormatter().date(from: "\(date ?? "")+0000") ?? Date()
    }
}
