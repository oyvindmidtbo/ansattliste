import Foundation

struct Coordinates {
    
    typealias Coordinate = (latitude: Double, longitude: Double)
    
    static let addressMap: [String: Coordinate] = [
        // Feil poststed. Det skal egentlig være Hundhamaren:
        "Olav Magnussonsveg 32B, 7562 Saksvik": Coordinate(63.4373429, 10.5962248), // Gøril Tronslien
        "Johan Ellingsens Gate 11A, 7562 Saksvik": Coordinate(63.4400043, 10.5940607), // Joakim Kallestad
        
        // Manuelle
        "Sigurd Hoels Vei 3, S14 / 503, 0655 Oslo": Coordinate(59.9167909, 10.7837654), // Jon Johansen
        "Trondheimsveien 101, 0565 Oslo": Coordinate(59.9245412, 10.772571), // Ørjan Breimo Helstrøm
        
        "Jens Bjelkes Gate 82a, 0652 Oslo": Coordinate(59.9123572, 10.7747658), // Andreas Moser
        "Rubina Ranas gate 5, 0190 Oslo": Coordinate(59.9113906, 10.7635729), // Ane Halse Sporastøyl
        "Spireaveien 4E, 0580 Oslo": Coordinate(59.9346883, 10.8018736), // Anna Li
        "Norderhovgata 38a, 0654 Oslo": Coordinate(59.9144108, 10.7789152), // Anna Regine Endal
        "Rosteds gate 3A, 0178 Oslo": Coordinate(59.9194332, 10.7508118), // August Heiervang Dahl
        "Brochmanns gate 14A, 0470 Oslo": Coordinate(59.9404675, 10.7649739), // Aurora Brun
        "Sandefjordgata 2B, 0464 Oslo": Coordinate(59.9393966, 10.7509873), // Asbjørn Ottesen Steinskog
        "Sandakerveien 16A, 0473 Oslo": Coordinate(59.9332346, 10.7600116), // Aryan Iranzamini
        "Nordbråtenveien 7a, 1410 Kolbotn": Coordinate(59.8230324, 10.80613), // Bente Johansson
        "Brannhaugen 1, 7026 Trondheim": Coordinate(63.3835594, 10.319057), // Bendik Solheim
        "Kroerveien 107B, 1435 ÅS": Coordinate(59.6482045, 10.8310181), // Cecilie Arebø
        "Bernt Balchens vei 18, 1364 Fornebu": Coordinate(59.8974381, 10.6240208), // Cathrine Bartolf Akre-Aas
        "Sofies gate 66B, 0168 Oslo": Coordinate(59.9282884, 10.7338501), // Cathinka Walberg
        "Sandakerveien 22f, 0473 Oslo": Coordinate(59.9343444, 10.7605088), // Daniel Romanich
        "Biskop Heuchs vei 48, 0871 Oslo": Coordinate(59.9472648, 10.7590474), // Eirin Mari Fossøy
        "Innherredsveien 55, 7068 Trondheim": Coordinate(63.436153, 10.4227848), // Aina Elisabeth Thunestveit
        "Tingvegen 32a, 7046 Trondheim": Coordinate(63.4274913, 10.4504308), // Eirik Jenssen Daling
        "Norbygata 21, 0187 Oslo": Coordinate(59.9138504, 10.7640039), // Abdifatah A. Bashi
        "Koefoedgeilan 2b, 7018 Trondheim": Coordinate(63.4295867, 10.3661842), // Andreas Mosti
        "Armauer Hansens gate 21, 0455 Oslo": Coordinate(59.9350307, 10.7408777), // Kent Andersen og Aksel Wester
        "Marstrandgata 13B, 0566 Oslo": Coordinate(59.9274156, 10.7683571), // Anders Refsdal Olsen
        "Åsveien 91, 1400 Ski": Coordinate(59.7145477, 10.8361088), // Anders Christensen
        "Trygves vei 7, 1473 Lørenskog": Coordinate(59.9245244, 10.9517595), // Asaf Khan
        "Sørumsvegen 46, 2022 Gjerdrum": Coordinate(60.0664718, 11.0871521), // Aksel Nilsen Huser
        "Fredensborgveien 21, 0177 Oslo": Coordinate(59.918647, 10.7484629), // Anders Erik Brustad
        "Theodor Løvstads vei 8, 0286 Oslo": Coordinate(59.9009553, 10.6834866), // Danuka Madduma Hewage
        "Herslebs gate 37, 0578 Oslo": Coordinate(59.9153302, 10.7675167), // Aleksander Sjåfjell
        "Dakota 6d, 7066 Trondheim": Coordinate(63.442235, 10.4377381), // Geir Sagberg
        "Bamrudvegen 29, 2093 Feiring": Coordinate(60.4853043, 11.1528936), // Frode Hansen
        "Badebakken 26 H201, 0467 Oslo": Coordinate(59.9464142, 10.7628872), // Andreas Bergman
        "Storgata 49F, 0182 Oslo": Coordinate(59.9157321, 10.7562622), // André Perzon
        "Vetlandsveien 82B, 0685 Oslo": Coordinate(59.8960721, 10.8443284), // Charlie Midtlyng
        "Pilestredet park 1, 0176 Oslo": Coordinate(59.920969, 10.7378952), // Gunvor Lemvik
        "Sandakerveien 84, 0484 Oslo": Coordinate(59.9447257, 10.7693232), // Anders Bøe
        "Kyavegen 21, 7045 Trondheim": Coordinate(63.432664, 10.4457816), // Erlend Løkken
        "Dannevigsveien 18B, 0463 Oslo": Coordinate(59.9373444, 10.7526689), // Andrea Holten
        "Sigrid Undsetsv. 5A, 7023 Trondheim": Coordinate(63.3907815, 10.3549527), // Eivind Sorteberg
        "Jerikoveien 33C, 1067 Oslo": Coordinate(59.9336932, 10.8845741), // Einar Afiouni
        "Årvollveien 58K, 0590 Oslo": Coordinate(59.9514445, 10.8175831), // Emil Øien Lunde
        "Lørenveien 64A, 0585 Oslo": Coordinate(59.9295757, 10.7980208), // Gaute Andreas Berge
        "Lysehagan 4, 0383 OSLO": Coordinate(59.9309126, 10.637462), // Frithjof Frederiksen
        "Sorgenfriveien 22, 7031 Trondheim": Coordinate(63.404934, 10.4009871), // Endre Waatevik
        "St Edmunds Vei 25, 0280 Oslo": Coordinate(59.9263896, 10.6715386), // Caroline Jenssen
        "Sverdrups Gate 4b, 0559 Oslo": Coordinate(59.9199866, 10.7625228), // Alexander Hanssen
        "Galgeberg 3K, 0657 Oslo": Coordinate(59.908043, 10.7804342), // Frøya Thue
        "Maridalsveien 33L, 0175 Oslo": Coordinate(59.9257613, 10.7508269), // Bendik Witzøe
        "Ringgata 4A, 0577 Oslo": Coordinate(59.9158529, 10.7740734), // Andreas Hagen Ulltveit-Moe
        "Fjellveien 24, 1532 Moss": Coordinate(59.4310405, 10.6635825), // Fredrik Matheson
        "Oppløpssiden 17, 2069 Jessheim": Coordinate(60.160755, 11.186394), // Fredrik Hansen
        "Grindbakken 36, 0764 Oslo": Coordinate(59.9695221, 10.647609), // Erlend Opdahl
        "Sigurds gate 11, 0650 Oslo": Coordinate(59.9122028, 10.7743363), // Elias Andreassen Thøgersen og Fredrik Alvsaker
        "Haugerudveien 88, 0674 Oslo": Coordinate(59.9212013, 10.8597849), // Even Arneberg Rognlien
        "Tangen Terrasse 50 A, 1456 Nesoddtangen": Coordinate(59.8571659, 10.6662433), // Fredrik Lillejordet
        "Gunnar Schjelderups vei 11D, 0484 Oslo": Coordinate(59.9486635, 10.7739824), // Fredrik Zander Kloster
        "Andersløkkeveien 14, 3514 Hønefoss": Coordinate(60.1673539, 10.3443318), // Even Holthe
        "Teglverksgata 7A, 0553 Oslo": Coordinate(59.9244456, 10.7655268), // Frida van der Drift Breivik
        "Ammerudhellinga 66, 0959 Oslo": Coordinate(59.9631541, 10.8756966), // Fredrik Løberg
        "Smedveien 1B, 1344 Haslum": Coordinate(59.9135472, 10.5616142), // Anne Berit Kigen Bjering
        "Væretrøa 20, 7055 Trondheim": Coordinate(63.4345609, 10.5852705), // Ane Mengshoel
        "Måltrostveien 92, 0786 Oslo": Coordinate(59.966614, 10.6474646), // Arne Magnus Mykkelbost
        "St. Jørgens vei 41, 0662 Oslo": Coordinate(59.9215522, 10.7969195), // Bendik Bjørndal Iversen
        "Apalstien, 1387 Asker": Coordinate(59.827101768710016, 10.45293989434537), // Anne Line Gjersem
        "Trollåsveien 7, 0672 Oslo": Coordinate(59.9077823, 10.8547065), // Arne Hatlen
        "Sinsenveien 83, 0586 Oslo": Coordinate(59.9387627, 10.797439), // Espen Storm Herseth
        "Skøyenveien 3, 0375 OSLO": Coordinate(59.9344791, 10.6928671), // Anja K. Lønningen
        "Jerikoveien 37B, 1067 Oslo": Coordinate(59.9334707, 10.8850627), // Frederik Heggenes
        "Dronning Eufemias gate 33, 0194 Oslo": Coordinate(59.9071202, 10.7585915), // Elias Nilsen Hvideberg
        "Knuts vei 21, 1456 Nesoddtangen": Coordinate(59.8548669, 10.6726092), // Bjørn-Ivar Strøm
        "Kværnerveien 12a, 1400 Ski": Coordinate(59.7267763, 10.8477198), // Brynjulv M. Brynjulvsen
        "Kitty kiellands vei 10E, 1344 Haslum": Coordinate(59.9155842, 10.5439884), // Bendik Segrov Ibenholt
        "Eli Sjursdotters vei 4C, 7021 Trondheim": Coordinate(63.410622, 10.3565296), // Camilla Jørmeland
        "Bidenkaps gate 2A, 0165 Oslo": Coordinate(59.9192407, 10.740927), // Bendik Edvardsen
        "Falbes gate 18C, 0170 Oslo": Coordinate(59.9221355, 10.7345845), // Andreas Storhaug Bøstrand
        "Thurmannsgate 12c, 0461 Oslo": Coordinate(59.9358565, 10.7524472), // Ben Høst
        "Valhallveien 9, 0196 OSLO": Coordinate(59.9007745, 10.7865889), // Hein Haraldsen
        "Simensbråtveien 8B, 1182 Oslo": Coordinate(59.8977647, 10.7889941), // Christian Mosveen
        "Jutulveien 8, 0852 OSLO": Coordinate(59.9431443, 10.738582), // Jan Thoresen
        "Hvalstadåsen 21A, 1395 Hvalstad": Coordinate(59.8564583, 10.4590137), // Carl Joachim Otvik
        "Gøteborggata 12D, 0566 Oslo": Coordinate(59.925505, 10.7670059), // Jan Gylta
        "Jupiterveien 33 A, 0489 Oslo": Coordinate(59.955325, 10.7868386), // Erik Haider Forsén
        "Waldemar Thranes gate 51C, 0173 Oslo": Coordinate(59.9268329, 10.747498), // Ingrid Asklund Larssen
        "Bestumveien 84G, 0283 Oslo": Coordinate(59.9176673, 10.6416391), // Bjørn Endresen
        "Magnus Åldstedts veg 3, 7024 Trondheim": Coordinate(63.3941109, 10.3407581), // Ingrid Hjulstad
        "Deichmans gate 27b, 0178 Oslo": Coordinate(59.9180273, 10.748466), // Hanna Bergljot Gimse
        "Nordre gate 22A, 0551 Oslo": Coordinate(59.9212846, 10.7586111), // Ingar Mathisen Almklov og Thea Mathisen Almklov
        "Dunkers gate 4c, 0357 Oslo": Coordinate(59.9256253, 10.7204887), // Bendik Sem Kvernevik
        "Industrigata 31B, 0357 Oslo": Coordinate(59.9255806, 10.7197885), // Ingrid Grønli Karoliussen
        "Sofies gate 75B, leil. 301, 0454 Oslo": Coordinate(59.9292786, 10.735727), // Isak Sunde Singh
        "Brinken 29 D, 0654 Oslo": Coordinate(59.9114175, 10.7792915), // Holger Ludvigsen
        "Lunden 20, 0598 Oslo": Coordinate(59.9386866, 10.830406), // Ida Christine Opsahl Høydal og Kenan Mahic
        "Pilestredet Park 25, 0176 Oslo": Coordinate(59.919229, 10.7370445), // Ida Bredal Skjørten og Johan Rusvik
        "Abels gate 20C, 7030 Trondheim": Coordinate(63.416146, 10.3944098), // Håvard Tollefsen
        "Øivinds Vei 7, 0590 Oslo": Coordinate(59.9436422, 10.8110404), // Håvard Ola Eggen
        "Hesselbergs gate 3B, 0555 Oslo": Coordinate(59.9276096, 10.7600106), // Ida Merete Enholm
        "Øvrefoss 10J, 0555 Oslo": Coordinate(59.9278761, 10.7578969), // Ingrid Volden
        "Dagaliveien 29B, 0783 Oslo": Coordinate(59.9576794, 10.6843258), // Ingeborg Sætersdal Sollid
        "Bankveien 36, 1362 Hosle": Coordinate(59.933565, 10.5964247), // Erik Wendel
        "Marie Wexelsens veg 6, 7045 Trondheim": Coordinate(63.4310871, 10.4569859), // Dag Stuan
        "Skarvaveien 77, 1350 Lommedalen": Coordinate(59.9460436, 10.4919001), // Dag Asbjørn Lotsberg
        "Lørenvangen 18, 0585 Oslo": Coordinate(59.9301101, 10.795777), // Christoffer Marcussen
        "Manglerudveien 14, 0678 Oslo": Coordinate(59.9007266, 10.8065752), // Daphne Leebeek
        "Dyre Halses Gate 9, 7042 Trondheim": Coordinate(63.4366818, 10.4129082), // Daniel Strømme Solberg
        "Dronning Eufemiasgate 12, 0191 Oslo": Coordinate(59.9082081, 10.7574276), // Citona Marie Rygg
        "Olaf Grilstads veg 12, 7025 Trondheim": Coordinate(63.4026002, 10.3390722), // Eivind Lie Andreassen
        "Urtegata 39, 0187 Oslo": Coordinate(59.9139307, 10.7653855), // Hege Haavaldsen
        "Bjørneveien 10, 3124 Tønsberg": Coordinate(59.2562404, 10.470523), // Jon Sindre Cosgrove
        "Hvidstenveien 13, 1396 Billingstad": Coordinate(59.8732016, 10.4836782), // Jarle Svendsrud
        "Sundvegen 2D, 2312 Ottestad": Coordinate(60.7803504, 11.0991865), // Eivind Brenningen
        "Stålverkskroken 30, 0661 Oslo": Coordinate(59.9190659, 10.7912572), // Jonas Løchsen og Kjersti Barstad Strand
        "Fayes gate 16, 0455 Oslo": Coordinate(59.9345266, 10.7415744), // Eivind Bergstøl
        "Nydalsveien 20A, 0484 Oslo": Coordinate(59.9488683, 10.7635368), // Henrik Walker Moe
        "Mor Go'Hjertas vei 23, H0507, 0469 Oslo": Coordinate(59.9425061, 10.7649891), // Eirik Årseth
        "Ivar Knutsons vei 36, 1161 OSLO": Coordinate(59.8684967, 10.7963421), // John Ringø
        "Kanonhallveien 48 K, 0585 Oslo": Coordinate(59.9323727, 10.7911337), // Eivind Mangset
        "Sars' gate 52, 0564 Oslo": Coordinate(59.9228203, 10.7729066), // Eirik Dahlen
        "Blåsbortveien 19A, 0873 OSLO": Coordinate(59.9521482, 10.7531641), // Jon Fageraas
        "Hartmanns vei 8B, 0284 Oslo": Coordinate(59.9227647, 10.6325522), // Christoffer Arntzen
        "Maridalsveien 33M, 0175 Oslo": Coordinate(59.9260613, 10.7508212), // Dagfinn Reinertsen
        "Løkebergveien 99, 1344 Haslum": Coordinate(59.9120278, 10.5692329), // Joakim Tysseng
        "Langbølgen 40, 1150 Oslo": Coordinate(59.8652123, 10.8151324), // Joel Chelliah
        "Aagots vei 18, 0687 Oslo": Coordinate(59.8905437, 10.8395177), // Johan Andre Lundar
        "Brynsveien 86A, 1346 Gjettum": Coordinate(59.9052878, 10.5136107), // Jens Andreas Huseby
        "Radka Toneffs vei 25A, 1410 Kolbotn": Coordinate(59.8205295, 10.8078673), // Johannes Bråthen Oma
        "Betzy Kjelsbergs vei 7, 0486 Oslo": Coordinate(59.9529779, 10.7750726), // Håvard Falk
        "Jens Bjelkes gate 6, 0562 Oslo": Coordinate(59.9198173, 10.763766), // Eivind Dagsland Halderaker
        "Sofienberggata 25A 2-502 H0504, 0558 Oslo": Coordinate(59.921745, 10.7628737), // Johannes Kvamme
        "Trostelia 17, 1388 BORGEN": Coordinate(59.8227482, 10.4140308), // Jarle Holtan
        "Hasselveien 14, 1344 Haslum": Coordinate(59.9233202, 10.5612523), // Elisabeth Rønneberg Lerfald
        "Stålverkskroken 34, 0661 Oslo": Coordinate(59.9194664, 10.7917904), // Eivind Reime
        "Lillian Byes veg 9, 7036 Trondheim": Coordinate(63.3881609, 10.4339765), // Jan Fredrik Wedén
        "Bjerregaards gate 29J, 0172 Oslo": Coordinate(59.9247408, 10.7460998), // Joachim Haakonsen
        "Gustav Wentzels vei 13B, 1383 Asker": Coordinate(59.8286111, 10.4191046), // Hong Nhung Thi Vo
        "Gunnar Schjelderups vei 11C, 0485 Oslo": Coordinate(59.9488539, 10.7738493), // Tore Lervik
        "Inges gate 4F, 0196 Oslo": Coordinate(59.9031782, 10.7762043), // Hilde Steinbru Heggstad
        "Allergotvegen 14b, 2053 Jessheim": Coordinate(60.1367463, 11.1762909), // Emil Døhlen Hansen
        "Staversletta 22B, 1341 Slependen": Coordinate(59.8825042, 10.471973), // Embla Belsvik
        "Havsteinflata 8G, 7021 Trondheim": Coordinate(63.4071556, 10.3614123), // Erik Gustafsson
        "Jordbærstien 21, 1405 Langhus": Coordinate(59.7672871, 10.8643416), // Gustav Karlsson
        "Stockholmgata 31, 0566 Oslo": Coordinate(59.9277143, 10.7709669), // Emilie Hallgren og Vetle Bu Solgård
        "Olav Nygaards vei 24, 0688 Oslo": Coordinate(59.8865551, 10.8530228), // Erik Mathisen
        "Øygardveien 16A, 1357 Bekkestua": Coordinate(59.9222024, 10.5791716), // Henrik Jackson
        "Sandakerveien 16F, H104, 0473 Oslo": Coordinate(59.9335406, 10.7598954), // Emil Staurset
        "Bjerregaards gate 19, 0172 Oslo": Coordinate(59.9244785, 10.7445365), // Guro Golberg Brøto
        "Blaklihøgda 16B, 7036 Trondheim": Coordinate(63.3951958, 10.4333468), // Lars Skrebergene
        "Maridalsveien 192, 0469 Oslo": Coordinate(59.9413515, 10.7620099), // Håkon Jarvis Drage
        "Spireaveien 6C, H0101, 0580 Oslo": Coordinate(59.9351518, 10.8005546), // Guro Johanson
        "Solhaugveien 91, 1337 SANDVIKA": Coordinate(59.8880268, 10.5127898), // Kristoffer Stensen
        "Vetlandsveien 63B, 0685 Oslo": Coordinate(59.900838, 10.8471316), // Lars Kinn Ekroll
        "Schweigaards gate 85, 0656 Oslo": Coordinate(59.9070851, 10.7748751), // Emil Säll Fuglerud
        "Ragna Nielsens Vei 30A, 0592 Oslo": Coordinate(59.9493708, 10.8254982), // Lene Valderhaug Bakke
        "Alunsjøveien 30G, 0957 Oslo": Coordinate(59.9614427, 10.8627211), // Espen Ekvang
        "Sandakerveien 16A U0104, 0473 Oslo": Coordinate(59.9332346, 10.7600116), // Halldis M. Søhoel
        "Grenseveien 34, 1406 Ski": Coordinate(59.7239403, 10.8095004), // Kristian Johannessen
        "Transistorfaret 2, 1396 Billingstad": Coordinate(59.8677182, 10.4838072), // Kristine Beddari Skjellestad
        "Haugerudveien 88, 0674  Oslo": Coordinate(59.9212013, 10.8597849), // Kristian Veøy
        "Konghellegata 10, 0570 Oslo": Coordinate(59.9295859, 10.7793876), // Kjetil Valle
        "Steinhammerveien 8D, 1177 Oslo": Coordinate(59.878491, 10.7872688), // Kristofer Giltvedt Selbekk
        "Hans Nielsen Hauges gate 33, 0481 Oslo": Coordinate(59.9387752, 10.7786271), // Kristine Steine
        "Hovseterveien 56B, 0768 Oslo": Coordinate(59.9497624, 10.6505671), // Knut Erik Langdahl
        "Lillevannsveien 6E, 0788 Oslo": Coordinate(59.9601747, 10.6610613), // Kristoffer Olsen
        "Stadsing Dahls gate 12, 7015 Trondheim": Coordinate(63.4304292, 10.4188532), // Halvor Lund
        "Karlstadveien 31F, 1516 Moss": Coordinate(59.425189, 10.6340522), // Harald Ringvold
        "Hovseterveien 98, 0768 Oslo": Coordinate(59.9519849, 10.6559778), // Henrik Sivertsgård
        "Øvrefoss 5, 0555 Oslo": Coordinate(59.9275678, 10.7577538), // Helena Van de Pontseele
        "Johan Castbergs vei 56, 0673 Oslo": Coordinate(59.9225477, 10.8681081), // Hanne Olsen
        "Rosenhoffgata 1A, 0569 Oslo": Coordinate(59.929044, 10.776943), // Hannah Williams Sollund
        "Paal bergs vei 25, 0692 Oslo": Coordinate(59.8749986, 10.8393555), // Henrik Johan Wistner
        "Munkerudåsen 19C, 1165 Oslo": Coordinate(59.8546255, 10.8084136), // Harald Krogh
        "Fougstads gate 25B, 0173 Oslo": Coordinate(59.9278182, 10.7472497), // Hans Kristian Henriksen
        "Dalerudveien 3, 0687 Oslo": Coordinate(59.8940158, 10.8510735), // Henning Håkonsen
        "Protonveien 19B, 0690 Oslo": Coordinate(59.8870483, 10.8376983), // Hege Melgård
        "Holmboes gate 7B, 0357 Oslo": Coordinate(59.9248985, 10.7197533), // Kjetil Svalestuen
        "Hans Nordahls gate 42, 0481 Oslo": Coordinate(59.9408771, 10.7790745), // Henriette Victoria Chiem
        "Valdresgata 17, 0557 Oslo": Coordinate(59.9296716, 10.7634418), // Linn Harbo Dahle
        "Balaklava 8B, 1513 Moss": Coordinate(59.4424629, 10.6414326), // Helene Vollene
        "Parkveien 1, 0350 Oslo": Coordinate(59.9224241, 10.731755), // Herman Møyner Lund
        "Slyngveien 26B, 0376 Oslo": Coordinate(59.9436425, 10.67728), // Henrik Gundersen
        "Gjevikbakkene 15, 1404 Siggerud": Coordinate(59.7758793, 10.9346179), // Karl Evald Wigestrand
        "Malerhaugveien 32B, 0661 Oslo": Coordinate(59.9164746, 10.794695), // Mari Langås
        "Observatorie Terrasse 9C, 0271 Oslo": Coordinate(59.912751, 10.7152795), // Maren Sofie Ringsby
        "Eilert Sundts gate 48B, 0355 Oslo": Coordinate(59.9243898, 10.7222496), // Henrietta Eide Bleness
        "Elgefaret 76C, 1362 Hosle": Coordinate(59.9398239, 10.6006438), // Henrik Wingerei
        "Adolph Tidemands Gate 29A, 2000 Lillestrøm": Coordinate(59.9571306, 11.0476363), // Herman Wika Horn
        "Stokkåsen 67, 7057 Jonsvatnet": Coordinate(63.4030896, 10.5037605), // Helene Nybøe
        "Bygdøylund 2, 0286 Oslo": Coordinate(59.8990063, 10.6848936), // Martha Dørum Jære
        "Sandefjordgata 2E, 0464 Oslo": Coordinate(59.9397489, 10.751507), // Martin Jackson
        "Pareliusveien 58, 1177 Oslo": Coordinate(59.8737123, 10.7852464), // Marte Ingeborg Lund
        "Trondheimsveien 105, 0565 Oslo": Coordinate(59.9246129, 10.7751852), // Mats Byrkjeland
        "Landingsveien 94, 0767 Oslo": Coordinate(59.9523044, 10.6505502), // Martin Remøy Solheim
        "Dovresvingen 27 b, 1184 Oslo": Coordinate(59.884525, 10.7986009), // Marte Ødegaard
        "Ensjøveien 6A, 0655 Oslo": Coordinate(59.9169066, 10.7839088), // Håkon Skaug Hesla
        "Fådveien 12A, 0751 Oslo": Coordinate(59.9397663, 10.6383282), // Marthe Skogen
        "Sigurd Hoels vei 112, 0655 Oslo": Coordinate(59.9185238, 10.7875913), // Mathilde Skylstad
        "Kongens Gate 9, H0506, 0153 Oslo": Coordinate(59.9101323, 10.7416949), // Mats Jonassen
        "Leirfivelveien 49, 1346 GJETTUM": Coordinate(59.9077214, 10.5260955), // Håkon Haga
        "Løkkedalsveien 20, 1444 Drøbak": Coordinate(59.6517041, 10.635705), // Julia Grundström
        "Tærudgata 12, 2004 Lillestrøm": Coordinate(59.9518467, 11.0470355), // Julie Hill Roa
        "Konows gate 8A, 0192 Oslo": Coordinate(59.9025547, 10.7724877), // Mathias Rørvik
        "Herman Foss' Gate 14A, 0171 Oslo": Coordinate(59.9283205, 10.7457152), // Herman Slyngstadli
        "Bjerregaards gate 31D, 0174 Oslo": Coordinate(59.9257538, 10.7469223), // Jonas Bakken
        "Bøkkerveien 14C, 0579 Oslo": Coordinate(59.9281125, 10.7964386), // Martine Enger
        "Holmenkollveien 78G, 0784 Oslo": Coordinate(59.9546896, 10.6645504), // Jonas Nouri
        "Jens Bjelkes gate 82 c, 0652 Oslo": Coordinate(59.9125218, 10.7746646), // Håkon Collett Bjørgan
        "Lilleakerveien 33 D, 0284 Oslo": Coordinate(59.9212342, 10.6379154), // Markus Rauhut
        "Tårnveien 5, 0369 OSLO": Coordinate(59.9337407, 10.706404), // Morten Enger Grønvold
        "Jordbærveien 95, 1161 Oslo": Coordinate(59.8705749, 10.7941141), // Unni Nyhamar Hinkel
        "Klæbuveien 212, leil. 104, 7037 Trondheim": Coordinate(63.3991015, 10.4088773), // Håkon Teigen Lund
        "Øraveien 7, 0760 Oslo": Coordinate(59.9454181, 10.6394933), // Tuva Foldøy Klingsheim
        "Utsiravegen 1A, 7045 Trondheim": Coordinate(63.4300481, 10.4425834), // Vebjørn Kvammen Beinnes
        "Parkveien 79, 0254 Oslo": Coordinate(59.9123065, 10.7186565), // Morten Kolstad
        "Nydalen allé 27, 0484 Oslo": Coordinate(59.9474788, 10.7665604), // Miriam Finjord
        "Alunsjøveien 30 G, 0957 Oslo": Coordinate(59.9614376, 10.862452), // June Henriksen
        "Darres gate 24, 0175 Oslo": Coordinate(59.9275281, 10.7530496), // Moquan Chen
        "Bjerregaards Gate 56A, 0174 Oslo": Coordinate(59.925897, 10.7465705), // Simen Ullern
        "Sørligata 8D, 0577 Oslo": Coordinate(59.9140266, 10.7688157), // Vegard Veiset
        "Bernt Lies Veg 1, 7024 Trondheim": Coordinate(63.3973537, 10.3452735), // Vegard Gamnes
        "Nydalsveien 20c, 0484 Oslo": Coordinate(59.9489296, 10.762908), // Jøran Vagnby Lillesand
        "Per Waalers vei 1, 1360 Fornebu": Coordinate(59.8992668, 10.6110455), // Tore Buer
        "Bjørnekollen 27, 1344 Haslum": Coordinate(59.9146535, 10.5651895), // Øyvind Skaar
        "Deør vei 72, 1555 Son": Coordinate(59.5273092, 10.702615), // Trond Arve Wasskog
        "Dakota 4B, 7066 Trondheim": Coordinate(63.4421723, 10.4386826), // Torgeir Thoresen
        "Langerudsvingen 30B, 1187 Oslo": Coordinate(59.876079, 10.8269089), // Torstein Gjengedal
        "Erika Nissens Gate 1, 0480 Oslo": Coordinate(59.9365157, 10.771873), // Toralf Christian Skaali Frich
        "Amtmann Meinichs Gate 20B, 0482 Oslo": Coordinate(59.9412612, 10.7754308), // Trygve Nybakk Vang
        "Ulvenveien 123C, 0665 Oslo": Coordinate(59.9212858, 10.8144016), // Trond Brynhildsvoll Tenfjord
        "Bøkkerveien 32 C, H0202, 0579 Oslo": Coordinate(59.9276022, 10.7926355), // Åshild Aaen Torpe
        "Trøavegen 34, 7224 Melhus": Coordinate(63.2693502, 10.2978863), // Øivind Fossan
        "Ilsvikøra 15, 7018 Trondheim": Coordinate(63.4317257, 10.3621374), // Kine Vaagan
        "Viktor Holens veg 18, 2074 Eidsvoll Verk": Coordinate(60.2830605, 11.1526788), // Øyvind Nerbråten
        "Skråstien 62, 7024 Trondheim": Coordinate(63.3973967, 10.2979327), // Øystein Grande Jaren
        "Aschehougs vei 29, 0587 Oslo": Coordinate(59.9487701, 10.7966378), // Øistein Hay Syversen
        "Hammerstadsgate 28, 0363 Oslo": Coordinate(59.9315516, 10.717843), // Julie Nyjordet Rossvoll
        "Kongsrudveien 4, 2015 Leirsund": Coordinate(59.9836606, 11.1451226), // Øyvind Hagen
        "3. Strøm Terrasse 21, 3046 Drammen": Coordinate(59.7333981, 10.1985745), // Jørgen Johansen
        "Kanonhallveien 62D, 0585 Oslo": Coordinate(59.9325858, 10.7913241), // Kaja Alexandra Dey
        "Sofienberggata 57B, 0563 Oslo": Coordinate(59.9203524, 10.7747607), // Øystein Knudsen
        "Seilduksgata 9B, 0553 Oslo": Coordinate(59.9252293, 10.7580057), // Øyvind Follan
        "Økernveien 180B, 0580 Oslo": Coordinate(59.9356682, 10.8079763), // Øyvind Stette Haarberg
        "Klommestensgata 40, 1533 Moss": Coordinate(59.4294871, 10.6719861), // Ketil Velle
        "Parkveien 13, 0350 Oslo": Coordinate(59.9218271, 10.7302504), // Magnus Rand og Tobias Skjelvik
        "Grinda 5b, 0861 Oslo": Coordinate(59.9655823, 10.7519752), // Linda Katrine Andresen
        "Hans Holts veg 18d, 2006 Løvenstad": Coordinate(59.9372117, 11.0133239), // Kim Stephen Bovim
        "Johan Svendsens gate 15, 0475 Oslo": Coordinate(59.9345308, 10.7721375), // Live Jacobsen
        "Signe Swenssons veg 2, 7036 Trondheim": Coordinate(63.3899698, 10.434407), // Kenneth Devik
        "Josefines gate 11B, 0351 Oslo": Coordinate(59.9244496, 10.727978), // Jørn Ola Birkeland
        "Hølandsgata 30, leilighet 202, 0655 Oslo": Coordinate(59.9108446, 10.7824801), // Jørund Amsen
        "Grefsenveien 34E, 0485 OSLO": Coordinate(59.9443602, 10.7747371), // Karianne Skudal Tjøm
        "Biterudveien 19B, 1383 Asker": Coordinate(59.8420005, 10.4257707), // Kathrine Løfqvist Kongshem
        "Ekebergåsen 42, 1900 Fetsund": Coordinate(59.9335757, 11.1898555), // Marianne Grov
        "Bregnefaret 17, 1405 Langhus": Coordinate(59.7660322, 10.8547661), // Marius Helstad
        "Olav Tryggvasons gate 13, 7011 Trondheim": Coordinate(63.4331582, 10.4006283), // Marius Thorvik
        "Havreveien 11, 0680 Oslo": Coordinate(59.8924825, 10.8110788), // Long Ngo
        "Fougstads gate 26, 0173 Oslo": Coordinate(59.9282269, 10.7473565), // Loritha Magnussen
        "Hans Nielsen Hauges gate 37f, 0481 Oslo": Coordinate(59.9384028, 10.7808035), // Magnus Sælensminde
        "Sandakerveien 22D, 0473 Oslo": Coordinate(59.9340016, 10.7603457), // Maren Schliekelmann Barth
        "Sandakerveien 22B, 0473 Oslo": Coordinate(59.9338378, 10.7611876), // Linda Prytz Sørlie
        "Jaktlia 18, 1361 Østerås": Coordinate(59.9454384, 10.6007606), // Linn Ramm Østgaard
        "Losbyveien 1 (H1002), 1475 Finstadjordet": Coordinate(59.9176572, 10.957909), // Mai Thao Charlotte Nguyen
        "Ullevålsveien 95C, 0359 Oslo": Coordinate(59.932878, 10.733159), // Mari Røysheim
        "Birkelundveien 62A, 1481 Hagan": Coordinate(60.0061856, 10.9299165), // Nordine Ben Bachir
        "Stensgata 34, 0358 Oslo": Coordinate(59.9320385, 10.7321132), // Morten Winther Wold
        "Toftes gate 23, 0556 Oslo": Coordinate(59.9289401, 10.760582), // Nicolai August Hagen
        "Stålverkskroken 38, 0661 Oslo": Coordinate(59.9190947, 10.79191), // Ninni Kim Nhi Hoang
        "Bratsbergvegen 112, 7036 Trondheim": Coordinate(63.3853368, 10.4282528), // Nikolas Rølland Hugsted
        "Toftes gate 24B, 0556 Oslo": Coordinate(59.9279315, 10.7614592), // Morten Olsen Osvik
        "Neptunveien 17G, 0493 Oslo": Coordinate(59.961259, 10.7790621), // Nils Andreas Baumgarten Skogstrøm
        "Freserveien 15, Leilighet 203, 0195 Oslo": Coordinate(59.9028379, 10.7859041), // Nicklas Utgaard
        "Colletts Gate 4A, 0169 Oslo": Coordinate(59.9242301, 10.7358031), // Nicolai Fredriksen
        "Plogveien 29, 0679 Oslo": Coordinate(59.8980636, 10.8108284), // Ole Reidar Holm
        "Dragonlia 29, 1396 Billingstad": Coordinate(59.8775534, 10.4732425), // Nora Futsæter
        "Seljeveien 7, 0575 Oslo": Coordinate(59.9284075, 10.7873823), // Ole Anders Stokker
        "Sigurds gate 3, 2004 Lillestrøm": Coordinate(59.9535139, 11.064158), // Ola Galde
        "Åsaveien 23, 0362 Oslo": Coordinate(59.9317291, 10.7214491), // Ragnhild Neset
        "Kapellveien 109, 1452 Nesoddtangen": Coordinate(59.8509401, 10.657131), // Yrjan Fraschetti
        "Schultz gate 10, 0365 Oslo": Coordinate(59.9282314, 10.721751), // Thea Thorleifsson
        "Utsiravegen 5, 7045 Trondheim": Coordinate(63.4303367, 10.4433395), // Peder Voldnes Langdal
        "Bispeluelia 10H, 1286 Oslo": Coordinate(59.8481192, 10.8167665), // Per Øyvind Kanestrøm
        "Porfyrveien 7B, 0753 Oslo": Coordinate(59.9439224, 10.6485939), // Ole Kristian Eidem Pedersen
        "Brundalsgrenda 10, 7058 Jakobsli": Coordinate(63.416951, 10.4822972), // Ole-Magnus Pedersen
        "Arups gate 22A, 0192 Oslo": Coordinate(59.906955, 10.771864), // Ole Kildehaug Furseth
        "Johan Sverdrups vei 27B, 0580 Oslo": Coordinate(59.9331136, 10.8112), // Per Christian Røine
        "Dælenenggata 16A, 0567 Oslo": Coordinate(59.9261051, 10.77242), // Peder Korsveien
        "Krokusveien 8c, 0875 Oslo": Coordinate(59.9588818, 10.737741), // Olav Folkestad
        "Hellinga 30, 1434 Ås": Coordinate(59.673127, 10.801026), // Oda Dahlen
        "Sandakerveien 16D C-603, 0473 Oslo": Coordinate(59.933273, 10.7598954), // Ola Claussen
        "Bjørnekollen 29, 1344 Haslum": Coordinate(59.914432, 10.5650146), // Ola Iuell Høklie
        "Solørgata 9, 0658 Oslo": Coordinate(59.9090442, 10.7874268), // Petter Astrup
        "Trostefaret 9, 0786 OSLO": Coordinate(59.9629678, 10.6574933), // Reidar Sande
        "Turbinveien 20, 0195 Oslo": Coordinate(59.9045268, 10.7881227), // Ragnhild Finsveen Liven
        "Grønnegata 7, 0350 Oslo": Coordinate(59.9215986, 10.7290983), // Vilde Brabrand Urfjell
        "Bekkeveien 15a, 1396 Billingstad": Coordinate(59.8775871, 10.4831054), // Veronika Marie Walnum
        "Teglverksgata 8, 0553 Oslo": Coordinate(59.924717, 10.7654217), // Ragnhild Aalvik og Vilde Graven Stokke
        "Lars Husmannsgate 17, 0598 Oslo": Coordinate(59.9349246, 10.8287508), // Philip Puente
        "Otto Blehrs vei 12, 0588 Oslo": Coordinate(59.9464755, 10.798664), // William Peer Berg
        "Engene 24, 1540 Vestby": Coordinate(59.5841339, 10.7764969), // Vegard Sund
        "Frognerveien 35b, 0266 Oslo": Coordinate(59.9197564, 10.7090579), // Viktor Grøndalen Solberg
        "Thorvald Meyers gate 17A, 0555 Oslo": Coordinate(59.9278409, 10.758824), // Vikki Nathalie Walle-Hansen
        "Freserveien 17, 0195 Oslo": Coordinate(59.9027787, 10.7862263), // Oscar Hafstad
        "Sandefjordgata 4N, 0464 Oslo": Coordinate(59.9401377, 10.751993), // Tobias R. Pettrém
        "Ragna Nielsens vei 17, 0592 Oslo": Coordinate(59.9474625, 10.825805), // Robert Graarup Kittilsen
        "Myrvangveien 4a, 1406 SKI": Coordinate(59.7193284, 10.8211103), // Svein Petter Gjøby
        "Lauritz Sands vei 13 A, 1359 Eiksmarka": Coordinate(59.9471079, 10.613812), // Rasmus Bauck
        "Bjørn Stallares vei 23, 0574 Oslo": Coordinate(59.9326744, 10.7814681), // Runar Skagestad
        "Siloveien 4, 0574 Oslo": Coordinate(59.9347628, 10.7811148), // Sigurd Rognhaugen
        "Sigurd Hoels Vei 61, 0655 OSLO": Coordinate(59.9175144, 10.7883688), // Safurudin Mahic
        "Lambertseterveien 52, 1154 Oslo": Coordinate(59.8761408, 10.8172027), // Simon Fagerli
        "Toftes Gate 39B, 0552 Oslo": Coordinate(59.924841, 10.7608479), // Robin Heggelund Hansen
        "Carl Jeppesens gate 24, 0481 Oslo": Coordinate(59.9395104, 10.7727236), // Sondre Larsen Ovrid
        "Bjørnheimveien 14A, 1086 Oslo": Coordinate(59.9413354, 10.915095), // Robert Austnes
        "Gunnar Schjelderups Vei 13E, 0485 Oslo": Coordinate(59.9496597, 10.776041), // Sebastian Cedrick Johansen
        "John Strandruds vei 5, 1366 Lysaker": Coordinate(59.9055719, 10.6242695), // Robert Larsen
        "Hofftunet 2, 0377 Oslo": Coordinate(59.9301942, 10.6746469), // Sara Waaler Eriksen
        "Jomfrubråtveien 43, 1179 Oslo": Coordinate(59.8897676, 10.7709486), // Thomas Svensen
        "Marienlundveien 6d, 1178 Oslo": Coordinate(59.8831569, 10.7829626), // Stian Daazenko
        "Andersbakkan 64, 7089 Heimdal": Coordinate(63.3488827, 10.3257731), // Runar Ovesen Hjerpbakk
        "Turbinveien 21, 0195 Oslo": Coordinate(59.9033001, 10.7875829), // Sindre Aubert
        "Løvsetfaret 42, 1188 Oslo": Coordinate(59.8727443, 10.8341932), // Sigve Vågsnes
        "Sandmovegen 43, 7093 TILLER": Coordinate(63.3349477, 10.3646603), // Thomas Torjuul Hansen
        "Hans Nordahls Gate 56, 0485 Oslo": Coordinate(59.9418949, 10.7786391), // Simen Johnsrud
        "Nydalen Allé 9, 0484 Oslo": Coordinate(59.9484775, 10.7657542), // Tia Firing
        "Sandakerveien 94, 0484 Oslo": Coordinate(59.9453985, 10.7702561), // Thor Kristian Valderhaug
        "Vangen 9B, 1163 Oslo": Coordinate(59.8660225, 10.8071571), // Tone Karlsen
        "Smålensgata 1A, 0657 OSLO": Coordinate(59.9072218, 10.7833766), // Simen Endsjø
        "Havegata 8B, 2010 Strømmen": Coordinate(59.9480193, 11.0129901), // Sindre Marken
        "Kalbakkveien 21, 0953 Oslo": Coordinate(59.9533997, 10.8769094), // Siri Olsen Corneliussen
        "Emilies vei 20b, 1434 Ås": Coordinate(59.6740673, 10.8095846), // Thomas Bråthen Fjeldberg-Norheim
        "Trondheimsveien 5B, leil. B219, 0560 Oslo": Coordinate(59.9393073, 10.7887974), // Rune Flobakk
        "Colletts gate 10B, 0169 Oslo": Coordinate(59.9252032, 10.7369274), // Signe Carlsen
        "Fernanda Nissens Gate 3D, 0484 Oslo": Coordinate(59.9457213, 10.7683575), // Simen Pjaaten
        "Fernanda Nissens Gate 7A, 0484 Oslo": Coordinate(59.9448964, 10.7668438), // Tiril Merethe Kyrkjeeide Solberg
        "Schweigaardsgate 64, 0656 Oslo": Coordinate(59.9080385, 10.7723061), // Tine Kleivane
        "Norderhovgata 20, 0654 Oslo": Coordinate(59.9131883, 10.7791896), // Tharald Jørgen Stray
        "Finns vei 2, 0575 Oslo": Coordinate(59.9242461, 10.7896937), // Sondre Gudmundsen Lea
        "Myrringen 28, 2052 Jessheim": Coordinate(60.1454769, 11.1957862), // Thomas Stenberg Oddsund
        "Kruttbuveien 6, 1482 Nittedal": Coordinate(60.0493124, 10.858471), // Steffen Hageland
        "Sagadammen 26, 0884 Oslo": Coordinate(59.9681775, 10.778197), // Therese Drivenes
        "Bølerskogen 25, 0691 Oslo": Coordinate(59.8811629, 10.8487735), // Svein Bendik Manum
        "Kurveien 15, 0495 Oslo": Coordinate(59.9590581, 10.7956726), // Sissel Fladby van Woensel Kooy
        "Havreveien 37, 0680 Oslo": Coordinate(59.8936414, 10.8130188), // Synne Fog
        "Nordåsveien 27, 1251 Oslo": Coordinate(59.8318812, 10.7902123), // Stephen Ramthun
        "Mølleveien 16, 3440 Røyken": Coordinate(59.7531374, 10.4105023), // Thea Sommersletta
        "Seljeveien 9, 0575 Oslo": Coordinate(59.9281415, 10.7878993), // Stian Gropen Søgård
        "Fougstads gate 2, 0173 Oslo": Coordinate(59.9259754, 10.7437491), // Sofie Parow
        "Risløkkveien 45B, 0583 Oslo": Coordinate(59.9318916, 10.8184171), // Sindre Nordbø
        "Herregårdsveien 125A, 1168 Oslo": Coordinate(59.8435777, 10.7767758), // Simon Engstrøm Nistad
        "Lakkegata 71B, 0562 Oslo": Coordinate(59.9187185, 10.7653356), // Simen Støa
        "Anna Sethnes Gate 4C, 0474 Oslo": Coordinate(59.932659, 10.7672576), // Simen Sæther Jørgensen
        "Bygdøy Allé 35 H0510, 0262 Oslo": Coordinate(59.9170337, 10.7099233), // Simon Foldvik
        "Herslebs gate 11, 0561 Oslo": Coordinate(59.9184338, 10.7631203), // Stian Stensli
        "Industrigata 69, 0357 Oslo": Coordinate(59.9286808, 10.7261869), // Jonas Mossin Wagle
        "Lørenveien 8, 0585 Oslo": Coordinate(59.9353966, 10.7853905), // Sonja Sarah Porter
        "Udbyes gate 4, 7030 Trondheim": Coordinate(63.4184707, 10.3944225), // Therese Hansen Federl
        "Gjeddevannsveien 13, 1911 Flateby": Coordinate(59.8350387, 11.1274139), // Stig-Rune Skansgård
        "Per O. Lunds Gate 8, H0401, 1475 Finstadjordet": Coordinate(59.9167751, 10.956281), // Sondre Martin Bakke
        "Solbergliveien 21, 0671 Oslo": Coordinate(59.9074567, 10.8388439), // Stefan Magnus Landrø
        "Skøyenåsveien 15 (H4093), 0686 Oslo": Coordinate(59.8976459, 10.8409654), // Sindre Moldeklev
        "Krebs' gate 16, 0478 Oslo": Coordinate(59.9360242, 10.7674724), // Frida Støvern
        "Haugmannsveien 18D, 0586 Oslo": Coordinate(59.9398925, 10.8041518), // Ole-Martin Mørk
        "Brennivegen 12, 2056 Algarheim": Coordinate(60.1436541, 11.2452984), // Thomas Eriksen
        "Åkebergveien 38A, 0650 Oslo": Coordinate(59.9099048, 10.7755801), // Stian Skulstad
        "Spireaveien 10B, 0580 Oslo": Coordinate(59.9337167, 10.8019264), // Thea Hove Johansen
        "Jens Bjelkes gate 15B, 0562 Oslo": Coordinate(59.9187205, 10.7645128), // Thea Svenkerud Rydjord
        "Vettreåsen 42, 1392 Vettre": Coordinate(59.8302198, 10.4816767), // Stine Marie Hjetland
        "Griffenfelds gate 1 A, 7012 Trondheim": Coordinate(63.429106, 10.3844182), // Janniche Alicabo Aarøen
        "Kurlandstien 42, 1052 Oslo": Coordinate(59.9428001, 10.8946508), // Anders Grytten Standal
        "Bjerkelundgata 4, 0553 Oslo": Coordinate(59.9250136, 10.7600341), // Frikk Hald Andersen
        "Peter Møllers vei 33, 0585 Oslo": Coordinate(59.9328701, 10.7958477), // Ole Magnus Lie
        "Ivan Bjørndals gate 15, 0472 Oslo": Coordinate(59.9384634, 10.7643984), // Tora Seim Gunstad
        "Maridalsveien 64, 0458 Oslo": Coordinate(59.9302636, 10.7536016), // Maria Kamara
        "Bispeluelia 10E, 1286 Oslo": Coordinate(59.8480007, 10.8167547), // Kristoffer Severinsen
        "Bøkkerveien 32A, 0587 Oslo": Coordinate(59.9276022, 10.7926355), // Joakim Gyllenskepp
        "Orionvegen 2A, 7037 Trondheim": Coordinate(63.3965505, 10.4131442), // Markus Lund
        "Lørenvangen 23C, 0585 Oslo": Coordinate(59.9307139, 10.7978016), // Ingeborg Halset Tollefsen
        "Østerliveien 33D, 1185 Oslo": Coordinate(59.8806889, 10.8104728), // Kevin Sillerud
        "Frognerseterveien 7D, 0775 Oslo": Coordinate(59.946917, 10.6964555), // Anders Håøy Rokne
        "Bjørnegårdsvingen 19, 1338 Sandvika": Coordinate(59.8945856, 10.5094237), // Halvor Boon Mundal
        "Fredrik glads gate 21B, 0482 Oslo": Coordinate(59.9422744, 10.7748942), // Elin Orsen
        "Vækerøveien 132E, 0383 Oslo": Coordinate(59.936231, 10.6366146), // Miina Lervik
        "Ekornåsen 6, 1362 Hosle": Coordinate(59.9389289, 10.6006115), // Jørn Hunskaar
        "Magnus' gate 11, seksjon 38, H0205, 0650 Oslo": Coordinate(59.9118604, 10.7746883), // Peter Rydberg
        "Granåsen 51D, 1362 Hosle": Coordinate(59.9394388, 10.5957334), // Erlend Heimark
        "Rådmann Hammers vei 4, 7020 Trondheim": Coordinate(63.4232255, 10.3564443), // Sigbjørn Lund Olsen
        "Christian Michelsens vei 45, 1464 Lørenskog": Coordinate(59.9426714, 10.9876112), // Kristian Rosland og Mathilde Wærstad
        "Mellomila 88, 7018 Trondheim": Coordinate(63.4321672, 10.3563059), // Marcus Haaland
        "Nordalveien 18, 0580 Oslo": Coordinate(59.9326507, 10.8135191), // Ingrid Fosså
        "Skådalsveien 13e, 0781 Oslo": Coordinate(59.9583803, 10.6951628), // Beate Ingebretsen
        "Freserveien 11, Leilighet 6015, 0195 Oslo": Coordinate(59.9023664, 10.7860307), // Alexander Svendsen
        "Jordal Terrasse 6, 0658 Oslo": Coordinate(59.9116607, 10.7877373), // Edvard Viggaklev Bakken
        "Brochmanns gate 8b, 0470 Oslo": Coordinate(59.9407336, 10.7632122), // William Høivik Feiring
        "Helgesens gate 84C, 0563 Oslo": Coordinate(59.9220571, 10.7768913), // Trym Sneltvedt
        "Ekraveien 44, 0567 Oslo": Coordinate(59.9556947, 10.6380149), // Katrine Hellem
        "Væretrøa 63, 7055 Ranheim": Coordinate(63.4308773, 10.5831226), // Linda Elisabet Østerberg
        "Stigen 2, 1435 Ås": Coordinate(59.6472144, 10.8318745), // Aslak Sheker Mkadmi
        "Trygve Nilsens vei 40C, 1061 Oslo": Coordinate(59.9356517, 10.9039262), // Daniel Hunn
        "Vettrelia 1, 1392 Vettre": Coordinate(59.8266831, 10.4678914), // Didrik Sæther
        "Trondheimsveien 42A, 0560 Oslo": Coordinate(59.9210294, 10.7689592), // Simen Fonnes
        "Thurmanns gate 10A, 0461 Oslo": Coordinate(59.9359856, 10.7548882), // Håvard Farestveit
        "Nedre Ila 5, 7018 Trondheim": Coordinate(63.4312825, 10.3718767), // Jostein Lilleløkken
        "Erlandstuveien 2B, 1178 Oslo": Coordinate(59.8884377, 10.7756033), // Kjetil Børs-Lind
        "Gørbitz' gate 3B, 0360 Oslo": Coordinate(59.9330528, 10.7273697), // Marlin Vanebo Tollefsen
        "Johan Scharffenbergs vei 91b (U0103), 0694 Oslo": Coordinate(59.8653633, 10.838943), // Dag Frode Solberg
        "Singsakerbakken 2A, 7030 Trondheim": Coordinate(63.4223381, 10.4000803), // Hanna Aksetøy Aalmen
        "Åsbakken 7, 7043 Trondheim": Coordinate(63.4328865, 10.4308004), // Emil Danielsen
        "Kristoffer Robins vei 14, 0978 Oslo": Coordinate(59.9546003, 10.9115856), // Erik Andreas Johansson
        "Emilie Kroghs veg 8B, 7023 Trondheim": Coordinate(63.3951834, 10.3593037), // Sigurd Hynne
        "Fredensborgveien 17, 0177 Oslo": Coordinate(59.9183749, 10.7479834), // Vilde Roksand Fauchald
        "Theodor Løvstads vei 20, 0286 Oslo": Coordinate(59.9012739, 10.6845615), // Hans Kristian
        "Trondheimsveien 26C, 0560 Oslo": Coordinate(59.9203656, 10.7670869), // Line Roarsdottir Hjertås
        "Rolf Hofmos gate 6, 0655 Oslo": Coordinate(59.9132039, 10.7867328), // Magnus Søyland
        "Klostergata 29, 7030 Trondheim": Coordinate(63.4226464, 10.3920426), // Kolbjørn Damien Kelly
        "Sørengkaia 64, 0194 Oslo": Coordinate(59.902618, 10.7560165), // Hieu Duc Pham
        "Siebkes gate 5B, 0562 Oslo": Coordinate(59.9182045, 10.766167), // Truls Hertzenberg Nergaard
        "Frydenbergveien 54, 0575 Oslo": Coordinate(59.929059, 10.7904226), // Frida Solheim
        "Ullernchausséen 6, 0379 Oslo": Coordinate(59.937199, 10.6810498), // Erlend Stølan
        "Fredrikke Qvams gate 25, 0172 Oslo": Coordinate(59.9252635, 10.7466461), // Astrid Kristine Ragnhildsdatter Bakken
        "Huitfeldts gate 47a, 0253 Oslo": Coordinate(59.9114813, 10.7209114), // Mari Solem Talmoen
        "Lørenveien 59A, 0585 Oslo": Coordinate(59.9290151, 10.797287), // Petter Daae
        "Blåbærskogen 5, 1412 Sofiemyr": Coordinate(59.7880845, 10.8152525), // Katarina Van de Pontseele
        "Elisenbergveien 3, 0265 Oslo": Coordinate(59.9181043, 10.7013885), // Adrian Dariusz Kortyczko
        "Osterhaus' gate 25, 0183 Oslo": Coordinate(59.9178095, 10.7513346), // Alfred Lieth Årøe
        "Sverdrups gate 17A, 0559 Oslo": Coordinate(59.9207371, 10.7663748), // Kristin Håberg
        "Gørbitz Gate 3A, 0360 Oslo": Coordinate(59.9330834, 10.727085), // Elena Snellingen
        "Helgesens gate 80E, 0563 Oslo": Coordinate(59.9223595, 10.7757803), // Markus Lavoll Gundersrud
        "Fredensborgveien 41, 0177 Oslo": Coordinate(59.9204193, 10.7495937), // Sigmund Bernhard Berbom
        "Harald Hårfagres gate 13, 7041 Trondheim": Coordinate(63.444899, 10.4433589), // Kåre Fosli Obrestad
        "Griffenfeldts gate 17E, 0460 Oslo": Coordinate(59.9335168, 10.7539209), // Filip Christoffer Larsen
        "Fossveien 16A, 0551 Oslo": Coordinate(59.9242987, 10.7550403), // Adrian Røstgård Flatner
        "Halvdan Svartes gate 4B, 0268 Oslo": Coordinate(59.9207369, 10.6929362), // Simen Holmestad
        "Seilduksgata 15A, 0553 Oslo": Coordinate(59.9253709, 10.7588363), // Anniken Rønvåg Syvertsen
        "Lavrans vei 18 h0401, 0670 Oslo": Coordinate(59.919214, 10.8413908), // Kari Eldfrid Stamnes
        "Thereses gate 3C, 0358 Oslo": Coordinate(59.9319423, 10.7339691), // Magnus Ramm
        "Berta Bråtens vei 7, 1362 Hosle": Coordinate(59.9421316, 10.5870093), // Bernt Julian Klevstad
        "Gøteborggata 14G, 0566 Oslo": Coordinate(59.9259354, 10.7673956), // Håkon Stubhaug Drangsland
        "Gladengveien 22, 0661 Oslo": Coordinate(59.9194167, 10.7935285), // Christian Scheie Hein
        "Brochmanns gate 8B, 0470 Oslo": Coordinate(59.9407336, 10.7632122), // Hedda Mathilde Sæther Langvik
        "Telemarksvingen 20, H0102, 0655 Oslo": Coordinate(59.9136075, 10.7847209), // Ragnhild Nordgård
        "Malmøgata 9, 0566 Oslo": Coordinate(59.9266066, 10.7675939), // William Andersson
        "Schwensens gate 16a, 0170 Oslo": Coordinate(59.9244109, 10.7386448), // Sara Hjelle
        "Ullernchausseen 46B, 0379 Oslo": Coordinate(59.9324838, 10.6692414), // Kristoffer Susort Kvale
        "Maridalsveien 39A, 0175 Oslo": Coordinate(59.9272068, 10.751132), // Nina Salvesen
        "Wilhelms gate 1, 0168 Oslo": Coordinate(59.9269466, 10.7325544), // Ingrid Vrålstad Løvås
        "Grevlingåsen 9C, 1362 Hosle": Coordinate(59.9384072, 10.5886445), // Sebastian Lindtvedt
        "Colletts Gate 63, 0456 Oslo": Coordinate(59.9311678, 10.7462478), // Helene Randi Behrens
        "Holmenveien 28G, 0374 Oslo": Coordinate(59.9443072, 10.6947645), // Ferdinand Gumø Løberg
        "Welhavens gate 3B, 0166 Oslo": Coordinate(59.9201505, 10.7339085), // Erika Åsberg
        "Maridalsveien 64F, 0458 Oslo": Coordinate(59.9307348, 10.7532866), // Birgitte Berg Swensson
        "Jomfrubråtveien 72D, 1179 Oslo": Coordinate(59.8853037, 10.7801558), // Kent Andersen
        "Toftes gate 32B, 0556 Oslo": Coordinate(59.9268384, 10.761258), // Simen Sælevik Tengs
        "Elisenbergveien 9b, 0265 Oslo": Coordinate(59.9182106, 10.7028965), // Sofie Tjønneland Urhaug
        "Norbygata 9, 0187 Oslo": Coordinate(59.9146851, 10.7627067), // Rebekka Lie
        "Senjaveien 43B, 9012 Tromsø": Coordinate(69.6445626, 18.9209616), // Richard Karlsen
        "Industrigata 57B, 0357 Oslo": Coordinate(59.9276105, 10.7239018), // Hedvig Johanne Berg
        "Klæbuveien 40B, 7030 Trondheim": Coordinate(63.4174212, 10.3980174), // Emil Telstad
        "Lars Husmanns Gate 3, 0598 Oslo": Coordinate(59.9343361, 10.8273367), // Erlend Johnsen Fossum
        "Nonnegata 6, 7014 Trondheim": Coordinate(63.4325904, 10.4119892), // Irnis Besirovic
        "Trondheimsveien 121, 0571 Oslo": Coordinate(59.9266808, 10.7778866), // Joakim Sjøhaug
        "Havstadvegen 7C, H0302, 7021 Trondheim": Coordinate(63.4065, 10.3548332), // Petter Sæther Moen
        "Frederik Glads Gate 24A, 0482 Oslo": Coordinate(59.9426266, 10.7747518), // Torbjørn Tessem
        "Munkerudveien 18B, 1163 OSLO": Coordinate(59.8636086, 10.8063475), // Geir Nygård
        "Nydalsveien 20C, 0484 Oslo": Coordinate(59.9489296, 10.762908), // Linda Christin Halvorsen
        "Gunnar Schjelderups vei 11c, 0485 Oslo": Coordinate(59.9488539, 10.7738493), // Kristina Cecilie Skåtun
        "Industrigata 31E, 0260 Oslo": Coordinate(59.9253622, 10.7193121), // Erling Van De Weijer
        "Arilds gate 10A, 7017 Trondheim": Coordinate(63.4272238, 10.3692177), // Bendik Aalmen Markussen
        "Kristiansands Gate 12C, 0463 Oslo": Coordinate(59.9388763, 10.7536447), // Christian Young
        "Nedre Grønberg 9, 7550 Hommelvik": Coordinate(63.4189672, 10.7776677), // Johannes Hoseth Klev
        "Bertrand Narvesens vei 7, 0661 Oslo": Coordinate(59.9195662, 10.7886794), // Thomas Dufourd
        "Langkroken 8b, 1394 Nesbru": Coordinate(59.8589661, 10.4905792), // Nina Inberg
        "Brettevilles gate 21, 0481 Oslo": Coordinate(59.9406998, 10.7737718), // Sarmilan Gunabala
        "Heimtrøa 24d, 7054 Ranheim": Coordinate(63.4187639, 10.5407431), // Emil Mork
        "Hans Barliens gate 1F, 0568 Oslo": Coordinate(59.9293785, 10.766761), // Sondre Widmark
        "Myrhaugen 25C, 0752 Oslo": Coordinate(59.9385269, 10.644102), // Nicolas Lopez
        "Stavsetsvingen 14, 7026 Trondheim": Coordinate(63.3844921, 10.3365277), // Pål Thomassen
        "Sorenskriver Ellefsens vei 39, 1443 Drøbak": Coordinate(59.6712525, 10.6272345), // Eirik Luka
        "Malerhaugveien 20C, 0661 Oslo": Coordinate(59.9146253, 10.7919838), // Nina Kjekstad
        "Schweigaardsgate 73, 0656 Oslo": Coordinate(59.9072382, 10.7729091), // Milena Przyborowska
        "Raukveien 1, 0680 Oslo": Coordinate(59.8934392, 10.8094328), // Daniel Berge
        "Markveien 21b, 0554 Oslo": Coordinate(59.9251007, 10.7568719), // Arvid Mildner
        "Maridalsveien 229B, 0467 Oslo": Coordinate(59.9446499, 10.7625071), // Ida Gylvik
        "Nordalveien 13C, 0580 Oslo": Coordinate(59.9319755, 10.8116748), // Andreas Heim
        "Ingeniørveien 16, 0196 Oslo": Coordinate(59.9025655, 10.7909478), // Kjersti Bjelkarøy
        "Arnljot Gellines vei 5b, 0657 Oslo": Coordinate(59.9052356, 10.7946589), // Linus Johansson
        "Arnljot Gellines vei 5B, 0657 Oslo": Coordinate(59.9052356, 10.7946589), // Mickel Hoang
        "Sandakerveien 23A, 0473 Oslo": Coordinate(59.9334333, 10.761224), // Daniel Hardang Lonar
        "Møllergata 47G H0501, 0179 Oslo": Coordinate(59.9176953, 10.7500531), // Anders Larsen
        "Hedmarksgata 8A, 0658 Oslo": Coordinate(59.909874, 10.7884908), // Torstein Mellingen Langan
        "Welhavens Gate 22C, 0350 Oslo": Coordinate(59.9210113, 10.7299103), // Lars Gjardar Musæus
        "Bernt Knudsens vei 43D, 1152 Oslo": Coordinate(59.8711236, 10.8039149), // Even Lindahl
        "Trondheimsveien 197a, 0570 Oslo": Coordinate(59.9349691, 10.7819805), // Sebastian Olafsson
        "Rektorhaugen 39, 0876 Oslo": Coordinate(59.9526356, 10.7475671), // Ole Marius Hammershaug
        "Waldemar Thranes gate 42A, 0171 Oslo": Coordinate(59.925981, 10.7446799), // Sivert Schou Olsen
        "Stensgata 8B, 0358 Oslo": Coordinate(59.9297997, 10.7323006), // Fredrik Kronholm Heiberg
        "Dælenenggata 38B, 0567 Oslo ": Coordinate(59.9285776, 10.7675416), // Sivert Johansen
        "Maridalsveien 30B, 0175 Oslo": Coordinate(59.9247816, 10.7500668), // Elias Nash Reksen
        "Vøyensvingen 14D, 0458 Oslo": Coordinate(59.9314888, 10.7531205), // Yngve Torgersen Milde
        "Helgesens gate 82C, 0563 Oslo": Coordinate(59.9220814, 10.7763251), // Magnus Lyngseth Vestby
        "Mor Go'Hjartas Vei 16, 0469 Oslo": Coordinate(59.9432027, 10.7646144), // Tobias G. Waaler
        "Durendalveien 1B, 1415 Oppegård": Coordinate(59.7665644, 10.8106703), // Yasmin El Mesbahi
        "Kruttørkeveien 23, 1482 Nittedal": Coordinate(60.0466111, 10.8581898), // Matias Vinjevoll
        "Grevlingveien 7b, 0595 Oslo": Coordinate(59.9448992, 10.8499049), // Halvor Hølmebakk Mangseth
        "Ånnerudskogen 123, 1383 Asker": Coordinate(59.8365773, 10.4114784), // Caroline Odden
        "Emil Korsmos vei 26, 0678 Oslo": Coordinate(59.8980943, 10.8099005), // Hermann Trampenau
        "Ullevålsveien 96B, 0451 Oslo": Coordinate(59.9352105, 10.7328137), // Jørgen Granseth
        "Helleveien 2E, 0376 Oslo": Coordinate(59.9419668, 10.6824135), // Mats Knutsen-Valen
        "Hammergjerdet 3, 7350 Buvika": Coordinate(63.3076721, 10.1603321), // Christian Øren
        "Sofies Gate 6D, 0170 Oslo": Coordinate(59.9219051, 10.7357174), // Brage Fosso
        "Nordskogvegen 3, 6412 Molde": Coordinate(62.7387724, 7.1400573), // Leif Erik Bjørkli
        "Hovinveien 37D, 0576 Oslo": Coordinate(59.9203515, 10.7908811), // Lena Tørresdal
        "Skomakerbakken 11b, 2074 Eidsvoll": Coordinate(60.2842829, 11.1547924), // Joakim Lindquister
        "Nardovegen 7B, Leilighet 36, 7032 Trondheim": Coordinate(63.4107922, 10.4153838), // Tor Martin Frøberg Wang
        "Hovinveien 37B, 0576 Oslo": Coordinate(59.9207057, 10.7906122), // Eirik Vigeland
        "Olav Aukrusts vei 4D, 0785 Oslo": Coordinate(59.9537671, 10.6627999), // Preben Ødegård Aas
        "Kjelsåsveien 107E, 0491 Oslo": Coordinate(59.9593732, 10.7878752), // Mats Mortensen
        "Kirkeveien 98B, 0361 Oslo": Coordinate(59.9327581, 10.724006), // Ruben Horn
        "Olaf Bulls vei 12, 0765 Oslo": Coordinate(59.9658111, 10.6441465), // Snorre Handeland Gryte
        "Landingsveien 21, 0770 Oslo": Coordinate(59.9502727, 10.6578619), // Madeleine Nathalie Sundby
        "Dalehaugen 6, 0657 Oslo": Coordinate(59.9066717, 10.7808292), // Markus Stige
        "Dronning Eufemias Gate 33, 0194 Oslo": Coordinate(59.9071202, 10.7585915), // Gabriel Wallin
        "Vakåsveien 82b, 1395 Hvalstad": Coordinate(59.8498706, 10.4542395), // Jenny Almestad
        "Måneveien 34, 1337 Sandvika": Coordinate(59.88549, 10.5084571), // Tri Nguyen
        "Uelands gate 57N, 0457 Oslo": Coordinate(59.9305346, 10.7494285), // Lukas Aasbø
    ]
}
