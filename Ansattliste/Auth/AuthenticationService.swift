import Auth0

struct AuthenticationService {
        
    private let appState: AppState
    private let scopes = "openid groups email app_metadata offline_access read:bekk"
    
    init(appState: AppState) {
        self.appState = appState
    }
    
    func authenticateAndDownloadEmployees() async {
        do {
            let credentials = try await Auth0
                .webAuth()
                .audience("https://bekk.eu.auth0.com/userinfo")
                .scope(scopes)
                .start()
            
            appState.setIsFirstUseAfterLogin()
            storeCredentials(credentials)
            appState.employeeService().fetchAndStoreEmployees()
        } catch {
            if !errorHasUserCancelledCode(error) {
                LogHelper.logError("Could not authenticate: \(error)")
            }
        }
    }
    
    private func errorHasUserCancelledCode(_ error: Error) -> Bool {
        if let webAuthError = error as? WebAuthError {
            return webAuthError == WebAuthError.userCancelled
        } else {
            return false
        }
    }
    
    func revokeCredentialsAndClearAppData() async {
        do {
            try await AuthenticationService.getCredentialsManager().revoke()
        } catch {
            LogHelper.logError("Could not revoke credentials")
            if !AuthenticationService.getCredentialsManager().clear() {
                LogHelper.logError("Could not clear credentials")
            }
        }
        
        await clearAppData()
    }
    
    func fetchBearerToken() async throws -> String {
        // Egentlig er det accessToken som skal brukes mot API-er, men ansatt-API-et er av ukjente grunner ikke satt opp slik (https://auth0.com/blog/why-should-use-accesstokens-to-secure-an-api)
        return try await AuthenticationService.getCredentialsManager().credentials(withScope: scopes).idToken
    }
    
    static func userExists() -> Bool {
        return getCredentialsManager().user != nil
    }
    
    private static func getCredentialsManager() -> CredentialsManager {
        return CredentialsManager(authentication: Auth0.authentication())
    }
    
    private func clearAppData() async {
        appState.setIsLoggedOut()
        appState.employeeService().clearEmployeeData()
    }
    
    private func storeCredentials(_ credentials: Credentials) {
        if AuthenticationService.getCredentialsManager().store(credentials: credentials) {
            appState.setIsLoggedIn()
        } else {
            LogHelper.logError("Could not store credentials")
        }
    }
}
