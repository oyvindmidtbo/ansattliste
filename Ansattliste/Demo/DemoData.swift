import Foundation

struct DemoData {
    static func getDemoEmployee(id: Int = 0) -> Employee {
        return Employee(
            department: "Technology",
            seniority: "Manager",
            division: "Bekk Consulting Oslo",
            practiceGroup: "Java",
            hireDate: ISO8601DateFormatter().date(from: "2017-08-03T00:00:00+0000") ?? Date(),
            id: id,
            title: "Systemutvikler",
            dateOfBirth: ISO8601DateFormatter().date(from: "1985-08-29T00:00:00+0000") ?? Date(),
            email: id > 0 ? "ola.nordmann\(id)@bekk.no" : "ola.nordmann@bekk.no",
            mobilePhone: "99999999",
            firstName: "Ola",
            lastName: id > 0 ? "Nordmann\(id)" : "Nordmann",
            fullName: id > 0 ? "Ola Nordmann\(id)" : "Ola Nordmann",
            streetAddress: "Veien 1",
            postalNr: "0101",
            postalAddress: "Oslo",
            showAddressInMapConsent: true,
            jobStatus: "I jobb",
            image: nil
        )
    }
    
    static func getDemoEmployees() -> [Employee] {
        return (1...10).map { getDemoEmployee(id: $0) }
    }
}
