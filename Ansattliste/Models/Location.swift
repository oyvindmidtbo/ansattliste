import CoreLocation

struct Location: Identifiable {
    let id = UUID()
    let fullName: String
    let address: String
    let coordinate: CLLocationCoordinate2D
}
