import Foundation

struct Filters {
    
    /// Avdeling
    static let design = "Design"
    static let managementConsulting = "Management Consulting"
    static let oppdrag = "Oppdrag"
    static let salgOgAdmin = "Salg & Admin"
    static let teknologi = "Teknologi"
    static let trondheim = "Trondheim"
    
    /// Senioritet
    static let trainee = "Trainee"
    static let konsulent = "Konsulent"
    static let senior = "Senior"
    static let manager = "Manager"
    
    /// Status
    static let ikkeStartet = "Ikke startet"
    static let iJobb = "I jobb"
    static let permisjon = "Permisjon/sykdom"
    
    var departmentFilters: [Filter]
    var seniorityFilters: [Filter]
    var jobStatusFilters: [Filter]
    
    init() {
        departmentFilters = [
            Filter(id: 1, name: Filters.design, isOn: true),
            Filter(id: 2, name: Filters.managementConsulting, isOn: true),
            Filter(id: 3, name: Filters.oppdrag, isOn: true),
            Filter(id: 4, name: Filters.salgOgAdmin, isOn: true),
            Filter(id: 5, name: Filters.teknologi, isOn: true),
            Filter(id: 6, name: Filters.trondheim, isOn: true),
        ]
        
        seniorityFilters = [
            Filter(id: 1, name: Filters.trainee, isOn: true),
            Filter(id: 2, name: Filters.konsulent, isOn: true),
            Filter(id: 3, name: Filters.senior, isOn: true),
            Filter(id: 4, name: Filters.manager, isOn: true),
        ]
        
        jobStatusFilters = [
            Filter(id: 1, name: Filters.ikkeStartet, isOn: true),
            Filter(id: 2, name: Filters.iJobb, isOn: true),
            Filter(id: 3, name: Filters.permisjon, isOn: true),
        ]
    }
    
    struct Filter: Identifiable {
        var id: Int
        var name: String
        var isOn: Bool
    }
}
