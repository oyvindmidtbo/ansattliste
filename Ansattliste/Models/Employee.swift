import Foundation

struct Employee: Identifiable, Codable, Comparable, Hashable {
    
    let department: String
    let seniority: String
    let division: String
    let practiceGroup: String
    let hireDate: Date
    let id: Int
    let title: String?
    let dateOfBirth: Date
    let email: String
    let mobilePhone: String
    let firstName: String
    let lastName: String
    let fullName: String
    let streetAddress: String
    let postalNr: String
    let postalAddress: String
    let showAddressInMapConsent: Bool
    let jobStatus: String
    var latitude: Double?
    var longitude: Double?
    var image: EmployeeImage?
    
    static func < (lhs: Employee, rhs: Employee) -> Bool {
        return lhs.fullName.compare(rhs.fullName, options: .caseInsensitive, locale: Locale(identifier: "nb_NO")) == .orderedAscending
    }
    
    static func == (lhs: Employee, rhs: Employee) -> Bool {
        return lhs.department == rhs.department
        && lhs.seniority == rhs.seniority
        && lhs.division == rhs.division
        && lhs.practiceGroup == rhs.practiceGroup
        && lhs.hireDate == rhs.hireDate
        && lhs.id == rhs.id
        && lhs.title == rhs.title
        && lhs.dateOfBirth == rhs.dateOfBirth
        && lhs.email == rhs.email
        && lhs.mobilePhone == rhs.mobilePhone
        && lhs.firstName == rhs.firstName
        && lhs.lastName == rhs.lastName
        && lhs.fullName == rhs.fullName
        && lhs.streetAddress == rhs.streetAddress
        && lhs.postalNr == rhs.postalNr
        && lhs.postalAddress == rhs.postalAddress
        && lhs.showAddressInMapConsent == rhs.showAddressInMapConsent
        && lhs.jobStatus == rhs.jobStatus
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(department)
        hasher.combine(seniority)
        hasher.combine(division)
        hasher.combine(practiceGroup)
        hasher.combine(hireDate)
        hasher.combine(id)
        hasher.combine(title)
        hasher.combine(dateOfBirth)
        hasher.combine(email)
        hasher.combine(mobilePhone)
        hasher.combine(firstName)
        hasher.combine(lastName)
        hasher.combine(fullName)
        hasher.combine(streetAddress)
        hasher.combine(postalNr)
        hasher.combine(postalAddress)
        hasher.combine(showAddressInMapConsent)
        hasher.combine(jobStatus)
    }
}
