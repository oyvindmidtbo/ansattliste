import UIKit

struct EmployeeImage: Codable {
    let imageData: Data?
    
    init(with image: UIImage) {
        imageData = image.pngData()
    }
    
    init(with data: Data) {
        imageData = data
    }
    
    func getImage() -> UIImage? {
        guard let imageData = imageData else {
            return nil
        }
        
        return UIImage(data: imageData)
    }
}
