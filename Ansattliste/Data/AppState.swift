import CoreData

final class AppState: ObservableObject {
    
    @Published var employees: [Employee] = []
    @Published var employeeFromSpotlight: Employee?
    @Published var showEmployeeFromSpotlight = false
    @Published var isLoggedIn = false
    @Published var isLoadingData = false
    @Published var isDemoMode = false
    @Published var isFirstUseAfterLogin = false
    @Published var downloadedEmployeeImages = 0
    
    let managedObjectContext: NSManagedObjectContext
    private var employeeServiceSingleton: EmployeeService?
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    init(managedObjectContext: NSManagedObjectContext, employees: [Employee]) {
        self.managedObjectContext = managedObjectContext
        self.employees = employees
    }
    
    func setEmployees(_ employees: [Employee]) {
        DispatchQueue.main.async {
            self.employees = employees
        }
    }
    
    func setIsLoggedIn() {
        DispatchQueue.main.async {
            self.isLoggedIn = true
        }
    }
    
    func setIsLoggedOut() {
        DispatchQueue.main.async {
            self.isLoggedIn = false
        }
    }
    
    func setIsLoadingData() {
        DispatchQueue.main.async {
            self.isLoadingData = true
        }
    }
    
    func setIsNotLoadingData() {
        DispatchQueue.main.async {
            self.isLoadingData = false
            self.isFirstUseAfterLogin = false
        }
    }
    
    func setIsFirstUseAfterLogin() {
        DispatchQueue.main.async {
            self.isFirstUseAfterLogin = true
        }
    }
    
    func resetNumberOfDownloadedEmployeeImages() {
        DispatchQueue.main.async {
            self.downloadedEmployeeImages = 0
        }
    }
    
    func incrementNumberOfDownloadedEmployeeImages() {
        DispatchQueue.main.async {
            self.downloadedEmployeeImages = self.downloadedEmployeeImages + 1
        }
    }
    
    func employeeService() -> EmployeeService {
        if employeeServiceSingleton == nil {
            let networkService = NetworkService(appState: self)
            let storageService = StorageService(managedObjectContext: self.managedObjectContext)
            let authenticationService = AuthenticationService(appState: self)
            let spotlightService = SpotlightService()
            let mapService = MapService(appState: self)
            
            employeeServiceSingleton = EmployeeService(appState: self, networkService: networkService, storageService: storageService, authenticationService: authenticationService, spotlightService: spotlightService, mapService: mapService)
            return employeeServiceSingleton!
        } else {
            return employeeServiceSingleton!
        }
    }
}
