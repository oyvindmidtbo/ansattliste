import CoreData

struct PersistentContainer {
    
    public static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    private static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Ansattliste")
        let storeURL = URL.storeURL(for: "group.ansattliste.core.data", databaseName: "Employees")
        let storeDescription = NSPersistentStoreDescription(url: storeURL)
        container.persistentStoreDescriptions = [storeDescription]
        
        container.loadPersistentStores(completionHandler: { storeDescription, error in
            if let error = error as NSError? {
                LogHelper.logError("Could not load persistentStores: \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    public static func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                LogHelper.logError("Could not save managedObjectContext: \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
