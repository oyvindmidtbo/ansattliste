import CoreData

struct StorageService {
    
    private let managedObjectContext: NSManagedObjectContext
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    func saveEmployees(employees: [Employee]) {
        managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        for employee in employees {
            createEmployeeEntity(employee: employee)
        }
        
        do {
            try managedObjectContext.save()
        } catch {
            LogHelper.logError("Could not save employees to Core Data: \(error)")
        }
    }
    
    func saveEmployeesForCallDirectory(employees: [Employee]) {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.persistentStoreCoordinator = managedObjectContext.persistentStoreCoordinator
        privateContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        privateContext.perform {
            for employee in employees {
                let callDirectoryEmployeeEntity = CallDirectoryEmployeeEntity(context: privateContext)
                callDirectoryEmployeeEntity.id = Int32(employee.id)
                callDirectoryEmployeeEntity.fullName = employee.fullName
                callDirectoryEmployeeEntity.number = employee.mobilePhone
            }
            
            do {
                try privateContext.save()
            } catch {
                LogHelper.logError("Could not save Call Directory employees to Core Data: \(error)")
            }
        }
    }
    
    func getAllEmployees() -> [Employee] {
        var employeeEntities = [EmployeeEntity]()
        
        do {
            employeeEntities = try managedObjectContext.fetch(EmployeeEntity.fetchRequest() as NSFetchRequest<EmployeeEntity>)
        } catch {
            LogHelper.logError("Could not get employees from Core Data: \(error)")
        }
        
        return Mappers.mapToEmployees(from: employeeEntities)
    }
    
    func getEmployee(with employeeId: Int) -> Employee? {
        let fetchRequest = EmployeeEntity.fetchRequest() as NSFetchRequest<EmployeeEntity>
        fetchRequest.predicate = NSPredicate(format: "id = %d", employeeId)
        
        var employeeEntity: EmployeeEntity?
        
        do {
            employeeEntity = try managedObjectContext.fetch(fetchRequest).first as EmployeeEntity?
        } catch {
            LogHelper.logError("Could not delete employees from Core Data: \(error)")
        }
        
        if let employeeEntity {
            return Mappers.mapToEmployee(from: employeeEntity)
        } else {
            return nil
        }
    }
    
    func getAllEmployeesForCallDirectory() -> [CallDirectoryEmployeeEntity] {
        var callDirectoryEmployeeEntities = [CallDirectoryEmployeeEntity]()
        
        do {
            callDirectoryEmployeeEntities = try managedObjectContext.fetch(CallDirectoryEmployeeEntity.fetchRequest() as NSFetchRequest<CallDirectoryEmployeeEntity>)
        } catch {
            LogHelper.logError("Could not get Call Directory employees from Core Data: \(error)")
        }
        
        return callDirectoryEmployeeEntities
    }
    
    func deleteAllEmployees() {
        do {
            try delete(with: EmployeeEntity.fetchRequest() as NSFetchRequest<NSFetchRequestResult>)
        } catch {
            LogHelper.logError("Could not delete employees from Core Data: \(error)")
        }
    }
    
    func deleteAllCallDirectoryEmployees() {
        do {
            try delete(with: CallDirectoryEmployeeEntity.fetchRequest() as NSFetchRequest<NSFetchRequestResult>)
        } catch {
            LogHelper.logError("Could not delete Call Directory employees from Core Data: \(error)")
        }
    }
    
    func deleteEmployee(with employeeId: Int) {
        let fetchRequest = EmployeeEntity.fetchRequest() as NSFetchRequest<NSFetchRequestResult>
        fetchRequest.predicate = NSPredicate(format: "id = %d", employeeId)
        
        do {
            try delete(with: fetchRequest)
        } catch {
            LogHelper.logError("Could not delete employee with id: \(employeeId) from Core Data: \(error)")
        }
    }
    
    func deleteCallDirectoryEmployee(with employeeId: Int) {
        let fetchRequest = CallDirectoryEmployeeEntity.fetchRequest() as NSFetchRequest<NSFetchRequestResult>
        fetchRequest.predicate = NSPredicate(format: "id = %d", employeeId)
        
        do {
            try delete(with: fetchRequest)
        } catch {
            LogHelper.logError("Could not delete Call Directory employee with id: \(employeeId) from Core Data: \(error)")
        }
    }
    
    private func delete(with fetchRequest: NSFetchRequest<NSFetchRequestResult>) throws {
        try managedObjectContext.execute(NSBatchDeleteRequest(fetchRequest: fetchRequest))
    }
    
    private func createEmployeeEntity(employee: Employee) {
        let employeeEntity = EmployeeEntity(context: managedObjectContext)
        
        employeeEntity.department = employee.department
        employeeEntity.seniority = employee.seniority
        employeeEntity.division = employee.division
        employeeEntity.practiceGroup = employee.practiceGroup
        employeeEntity.hireDate = employee.hireDate
        employeeEntity.id = Int32(employee.id)
        employeeEntity.title = employee.title
        employeeEntity.dateOfBirth = employee.dateOfBirth
        employeeEntity.email = employee.email
        employeeEntity.mobilePhone = employee.mobilePhone
        employeeEntity.firstName = employee.firstName
        employeeEntity.lastName = employee.lastName
        employeeEntity.fullName = employee.fullName
        employeeEntity.streetAddress = employee.streetAddress
        employeeEntity.postalNr = employee.postalNr
        employeeEntity.postalAddress = employee.postalAddress
        employeeEntity.showAddressInMapConsent = employee.showAddressInMapConsent
        employeeEntity.jobStatus = employee.jobStatus
        employeeEntity.image = employee.image?.imageData
    }
}
