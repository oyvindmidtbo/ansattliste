import Foundation

struct EmployeeDTO: Codable {
    let id: Int
    let title: String?
    let dateOfBirth: String?
    let email: String?
    let mobilePhone: String?
    let gender: String?
    let firstName: String?
    let lastName: String?
    let name: String?
    let consent: Bool?
    let streetAddress: String?
    let postalNr: String?
    let postalAddress: String?
    let dependentsName: String?
    let dependentsMobileNumber: String?
    let externalLoginId: String?
    let spouse: Bool?
    let yearsOfPreviousExperience: Double?
    let showAddressInMapConsent: Bool?
    let departmentId: Int?
    let department: String?
    let seniorityId: Int?
    let seniority: String?
    let divisionId: Int?
    let division: String?
    let practiceGroupId: Int?
    let practiceGroup: String?
    let hrManagerId: Int?
    let hrManager: String?
    let jobStatus: String?
    let jobStatusId: Int?
    let hireDate: String?
    let resignationDate: String?
}
