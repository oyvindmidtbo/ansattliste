import SwiftUI

@main
struct AnsattlisteApp: App {
    
    @Environment(\.scenePhase) private var scenePhase
    private let context = PersistentContainer.context
    private let appState: AppState
    
    init() {
        appState = AppState(managedObjectContext: context)
        
        guard isNotRunningUnitTests() else {
            return
        }
        
        if AuthenticationService.userExists() {
            appState.isLoggedIn = true
            appState.employees = appState.employeeService().getLocalEmployees()
            appState.employeeService().fetchAndStoreEmployees()
        } else {
            appState.employeeService().clearEmployeeData()
        }
    }
    
    private func isNotRunningUnitTests() -> Bool {
        NSClassFromString("XCTestCase") == nil
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, context)
                .environmentObject(appState)
        }
        .onChange(of: scenePhase) { phase in
            if phase == .background {
                PersistentContainer.saveContext()
            }
        }
    }
}
