### Ansattliste
Dette er en iOS-app som viser alle de ansatte i Bekk med diverse informasjon som hentes fra ansatt-API-et. Denne appen er publisert som en _unlisted app_, som betyr at den ikke er søkbar på App Store, men tilgjengelig via https://apps.apple.com/app/bekk/id1658258254.

#### Funksjoner
- Liste opp alle ansatte.
- Se utfyllende informasjon og bilde av hver enkelt ansatt.
- Kart over alle ansatte som har samtykket til synlighet på kartet i hvem-hva-hvor.
- Integrasjon med Spotlight-søket på iOS.
- Integrasjon med CallKit på iOS som gjør at man kan se navnet til den som ringer selv om man ikke har vedkommende i kontaktlisten.

#### Xcode Cloud
Appen er satt opp med CI/CD i Xcode Cloud. Det blir kjørt enhetstester når man merger til `master`, og det bygges en ny versjon som publiseres på App Store Connect når man merger til `release`. Xcode Cloud er gratis frem til desember 2023, deretter $14,99 per måned.

#### Publisering
For å publisere appen må den lastes opp til App Store Connect. Det gjøres enklest ved hjelp av Xcode Cloud, men kan også gjøres manuelt.

##### Xcode Cloud
- Oppdater _Version_ og _Build_ i alle targets (Ansattliste og CallDirectory) i prosjektet.
- Push endringene på `master`.
- Lag pull request fra `master` til `release` og merge denne.

##### Manuelt
- Oppdater _Version_ og _Build_ i alle targets (Ansattliste og CallDirectory) i prosjektet.
- Product &rarr; Archive (krever at man har valgt en fysisk enhet oppe til venstre i Xcode).
- Distribute App &rarr; App Store Connect og last opp.

Gjør følgenden når appen er lastet opp App Store Connect:

- Velg fanen _App Store_.
- Trykk på plusstegnet ved siden av _iOS App_ og skriv inn det nye versjonsnummeret.
- Fyll ut _What's New in This Version_.
- Velg bygget som er lastet opp under _Build_.
- Lagre og send inn til review.

#### Auth0
For å logge inn i appen brukes en Auth0 SDK. I tillegg er det definert en Auth0-app i Auth0-dashboardet til Bekk. Les mer her: https://github.com/auth0/Auth0.swift

#### Ting som kan fikses/forbedres
##### Koordinater
For å vise de ansatte på et kart må adressene konverteres til koordinater (latitude og longitude). Dette gjøres ved hjelp av funksjonen `geocodeAddressString(_ addressString: String)` på `CLGeocoder`. Denne støtter kun et visst antall oppslag per minutt, slik at den feiler dersom man gjør dette oppslaget med alle ansatte hver gang man åpner appen. Derfor har disse koordinatene blitt hardkodet i `Coordinates.swift` og oppdatert med jevne mellomrom. Når det blir gjort oppslag på adresser som ikke ligger i denne fila blir det skrevet ut et innslag i loggen som kan kopieres over i fila, f.eks.:
```"Slottsplassen 1, 0010 Oslo": Coordinate(59.9170455, 10.7251882), // Kong Harald```

Det finnes flere bibliotek som kan brukes for å konvertere fra adresser til koordinater, men felles for de fleste (alle?) er at de har et tak på hvor mange forespørsler man kan gjøre i måneden uten å betale for det. Mulige løsninger er å sette opp en egen backend som kan gjøre konverteringen, eller å utvide ansattliste-API-et til å også sende med koordinater.

##### Kodeforbedring
Funksjonen `getLocations()` i `MapService` kjører veldig mange ganger. Den bør bare kjøres én gang når man navigerer til kartsiden.

##### macOS-app
Appen er tilgjengelig som en macOS-app på enheter med Apple Silicon, men er ikke optimalisert for denne plattformen. Det er mest sannsynlig ikke så mye arbeid for å få den til å se bedre ut der.

##### Oppstartsskjerm
`LaunchScreen.Storyboard` er gammeldags og kan byttes ut med noe mer moderne: https://betterprogramming.pub/launch-screen-with-swiftui-bd2958771f3b